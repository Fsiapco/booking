<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ListOfRoomsController@create');

// Route::get('/', function () {
//     return view('sample');
// });

Auth::routes(['register' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin',function(){
    return view('layouts.admin');
});

// Room and booking
Route::resource('room', 'RoomController');
Route::get('allinreserved','ReservationController@reserved');
Route::get('bookedclient','ReservationController@booked');
Route::resource('reservation', 'ReservationController', ['except' => 'show']);
Route::get('reservation/{id}/{checkIn}/{checkOut}', 'ReservationController@show')->name('reservation.show');
Route::post('room/getRoom', 'RoomController@getRoomDetails')->name('room.getRoomDetails');
Route::get('room/getDetails/{id}', 'RoomController@getRoomDetailsId');
Route::get('room/check-availability/{check_in}/{check_out}', 'RoomController@checkAvailable')->name('room.check');
Route::get('datepicker', function(){
    return view('room.check');
});
Route::get('/payment/success', 'ReservationController@paymentSuccess');
Route::get('/payment/refund', 'ReservationController@refundTransaction');
Route::resource('insert', 'InsertController');
Route::post('paypal/notify', 'ReservationController@notify');
Route::post('tourPackage','ReservationController@tourPackageData');


//List of Rooms
Route::resource('list','ListOfRoomsController');
Route::get('session/destroyer', 'ReservationController@sessionDestroy');
Route::get('getDates/{id}', 'ReservationController@getDates');
Route::get('/getAvaiableRooms/{checkIn}/{checkOut}', 'RoomController@getAvailableRooms');
Route::get('/updateCheckStatus/{id}/{check}','ReservationController@updateCheckinStatus');
Route::get('contactUs','API\ContactUsController@store');

//get all Package
Route::get('getPackageDates/{id}', 'ReservationController@PackageDates');
Route::get('getAvailableRooms/{id}','ReservationController@AvailableRoom');
Route::get('getbookRoom/{id}','ReservationController@bookedRoom');
//search
Route::post('searchReservation','ReservationController@searchforResearved');
Route::get('reserveDetails/{id}','ReservationController@detialsofResearved');



Route::get('roomDetails/{id}','ListOfRoomsController@roomDetails');
Route::get('{path}','HomeController@index')->where('path','([A-z\d\/_.]+)?');
Route::get('/test1', function(){
    return \App\OtherRoomPhoto::all();
});

Route::post('quotation-Page','QuotationController@send');



//catering
Route::get('catering-Service', function(){
    return view('content.catering');
});

// tour package
Route::get('tour-Package', function(){
    $package = \App\Package::all();
    return view('content.tourPackage',compact('package'));
});

//ajax details
Route::get('tourPackage-Modal/{id}','RoomDetailsController@packageDetails');
Route::get('Room-Modal/{id}','RoomDetailsController@roomDetails');

// van package
Route::get('van-transport', function(){
    return view('content.vanTransport');
});

// FUNCTIONS & EVENTS (quotation)
Route::get('functions-quotation', function(){
    return view('content.quotation');
});

//press release
 Route::get('press-release', function(){
    return view('content.pressRelease');
});

// locale (attraction)
Route::get('locale-attraction', function(){
    return view('content.localAttraction');
});

// locale (delicacies)
Route::get('locale-delicacies', function(){
    return view('content.localDelicacies');
});
//quotation
Route::get('quotations', function(){
    return view('content.quotation');
});
// locale (events)
Route::get('locale-events', function(){
    return view('content.localEvents');
});

// terms
Route::get('terms', function(){
    $terms = \App\terms::all();
    return view('content.terms',compact('terms'));
});

//garden-resort
Route::get('garden-resort', function(){
    $amenities = \App\Amenities::where('branch_id',1)->get();
    $room = \App\Room::with('room_amenities','room_details')->where('branch_id',1)->get();
    $guestRequest = \App\ExtraRequest::all();

    return view('content.gardenResort', compact('amenities','room','guestRequest'));
});

//beach-resort
Route::get('beach-resort', function(){
    $amenities = \App\Amenities::where('branch_id',2)->get();
    $room = \App\Room::with('room_amenities','room_details')->where('branch_id',2)->get();
    $guestRequest = \App\ExtraRequest::all();
    return view('content.beachResort', compact('amenities','room','guestRequest'));
});


Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

//payment
Route::get('PackagePayment',function(){
    return view('payment.tourPackagePayment');
});
