<div>
    <h4>Name of Quotation Requester:</h4>
        <span>
            {{$data['name'] }}
        </span>
</div>
<div>
    <h4>Email:</h4>
        <span>
            {{$data['email'] }}
        </span>
</div>
<div>
    <h4>Phone number:</h4>
        <span>
            {{$data['number'] }}
        </span>
</div>
<div>
    <h4>Type of Event:</h4>
        <span>
            {{$data['type'] }}
        </span>
</div>
<div>
    <h4>Date of Event:</h4>
        <span>
            {{$data['date'] }}
        </span>
</div>
<div>
    <h4>Time of Event:</h4>
        <span>
            {{$data['time'] }}
        </span>
</div>
<div>
    <h4>No. of Pax:</h4>
        <span>
            {{$data['pax'] }}
        </span>
</div>
<div>
    <h4>Remarks:</h4>
        <span>
            {{$data['remarks'] }}
        </span>
</div>