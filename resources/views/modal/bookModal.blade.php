 <!-- Modal -->
 <form action="{{ route('reservation.store') }}" method="POST" >
    @csrf
   <div class="modal fade" id="bookNowModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
       <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
       <div class="modal-content">
           <div class="modal-header text-white" style="background-image: linear-gradient(to right, #410064 , violet);">
               <h5 class="modal-title" id="exampleModalLongTitle">Confirm Your Reservation</h5>
               <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
           </div>
           <div class="modal-body p-0">
               <div class="text-center modalDate p-4">
                   {{-- Booking Details --}}
                   {{ Form::hidden('dates', '', ['id' => 'booking_dates']) }}
                   {{ Form::hidden('room_detail_id', $rooms->room_details->first()->id, ['id' => 'room_id']) }}
                   {{ Form::hidden('total_cost', old('total_cost'), ['id'=>'total_cost']) }}
                   {{ Form::hidden('charges', '', ['id'=>'charges']) }}

                   
                   {{-- User Details --}}
                   {{ Form::hidden('firstname', '', ['id'=>'formFirstName']) }}
                   {{ Form::hidden('lastname', '', ['id'=>'formLastName']) }}
                   {{ Form::hidden('contact_number', '', ['id'=>'formContactNumber']) }}
                   {{ Form::hidden('email', '', ['id'=>'formEmail']) }}
                   {{ Form::hidden('nationality', '', ['id'=>'formNationality']) }}
                   {{ Form::hidden('special_request', '', ['id'=>'formSpecialRequest']) }}
                   {{ Form::hidden('night', '', ['id'=>'night']) }}
                   {{ Form::hidden('priceNight', $rooms->price, ['id'=>'priceNight'])  }}
                   {{ Form::hidden('paymentMethod', '', ['id'=>'formpaymentofMethod'])  }}

                   <i class="fas fa-calendar-alt"></i>
                   {{-- June 06 --}}{{\Carbon\Carbon::parse($checkIn)->format('M d')}}
                   <i class="fas fa-angle-right ml-2 mr-2"></i>
                   <i class="fas fa-calendar-alt"></i>
                   {{-- June 10 --}}{{\Carbon\Carbon::parse($checkOut)->format('M d')}}
                   <i class="fas fa-angle-right ml-2 mr-2"></i>
                   <i class="fas fa-moon"></i>
                   <span class="noOfNights"></span> &nbsp;Nights                        
               </div>
               <div class="row p-2">

                   {{-- Customer Details --}}
                   <div class="col-md-6">
                       <div class="text-center p-3 h4 font-weight-bold">
                            Your Details
                       </div>
                       <div class="pl-3 pr-3 font-weight-bold">
                          Name:
                       </div>
                       <div class="pl-5 pr-3 font-weight-bold" style="font-size:18px;color:#4B0082">
                           <span class="text-capitalize" id="displayFname"></span>
                           <span class="text-capitalize" id="displayLname"></span>
                       </div>
                       <div class="pl-3 pr-3 font-weight-bold">
                          Contact #:
                       </div>
                       <div class="pl-5 pr-3 font-weight-bold" style="font-size:18px;color:#4B0082">
                          <span id="displayContact"></span>
                       </div>
                       <div class="pl-3 pr-3 font-weight-bold">
                          Email:
                       </div>
                       <div class="pl-5 pr-3 font-weight-bold" style="font-size:18px;color:#4B0082">
                          <span id="displayEmail"></span>
                       </div>
                       <div class="pl-3 pr-3 font-weight-bold">
                        Nationality:
                        </div>
                        <div class="pl-5 pr-3 font-weight-bold" style="font-size:18px;color:#4B0082">
                            <span id="displayNationality"></span>
                        </div>
                        <div class="pl-3 pr-3 font-weight-bold">
                            Mode of Payment:
                        </div>
                        <div class="pl-5 pr-3 font-weight-bold text-justify" style="font-size:18px;color:#4B0082">
                                <span id="displaypaymentMethod">%</span>
                        </div>  
                       <div class="pl-3 pr-3 font-weight-bold">
                          Special Request:
                       </div>
                       <div class="pl-5 pr-3 font-weight-bold text-justify" style="font-size:18px;color:#4B0082">
                               <span id="displayRequest"></span>
                       </div>  
                     
                       
                   </div>


                   {{-- Reservation Details --}}
                   <div class="col-md-6 reservation_detials" style="border-left: #E1D8CF solid 1px;">
                       <div class="text-center p-3 h4 font-weight-bold">
                            Reservation Details
                       </div>
                       <div class="modalDate pl-1 text-dark">
                               <span class="font-weight-bold">Select Room/Rates</span> 
                           </div>
                           <div class="pl-2 pr-4 font-weight-bold">
                               <span style="color:indigo">{{ $rooms->type }} #{{ $rooms->room_details->first()->room_no }}</span><span class="float-right"><span style="color:#61B361">&#8369;{{ $rooms->price }}</span>/Night</span>
                           </div>
                           <div class="pl-2 pr-4 font-weight-bold" style="font-style:italic;">
                               Not refundable
                           </div>
                          
                           <div>
                               <span class="pl-2 pr-4 font-weight-bold" style="font-style:italic;">Tax Inclusive</span>
                           </div>
                           <div class="mb-2">
                               <span class="pl-2 pr-4 font-weight-bold">Sub Total </span><span class="float-right pr-4" style="color:#61B361 !important;font-weight:bold;font-size:20px;" id="displayprice"></span>
                               <span class="float-right" style="color:#61B361 !important;font-weight:bold;font-size:20px;">&#8369; </span>
                           </div>
                           <br>
                           <br>
                           <hr>
                           
                           <div class="pl-2 pr-4 font-weight-bold" style="font-style:italic;">
                            Additional Charge(Request) <span class="float-right price " style="color:#61B361 !important;font-weight:bold;font-size:15px;" id="requestPrice">0</span> 
                            <span class="float-right " style="color:#61B361 !important;font-weight:bold;font-size:15px;">&#8369;</span> 
                            </div>
                           <div class="pl-2 pr-4 font-weight-bold" style="font-style:italic;">
                               <span class="pr-4 font-weight-bold">Total Amount</span><span class="float-right price" style="color:#61B361 !important;font-weight:bold;font-size:25px;" id="displayTotal"></span> 
                               <span class="float-right " style="color:#61B361 !important;font-weight:bold;font-size:25px;">&#8369; </span> 
                           </div>
                   </div>

                   <div class="col-12 m-2 ">
                            <input type="checkbox" name="" id="termS"> I hereby agree to the <button type="button"  data-toggle="modal" data-target="#TermsAndConditionModal">Terms and Condition</a> 
                   </div>
               </div>
              
           </div>
           <div class="modal-footer">
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
           <button type="submit" class="btn text-white payBtn"   style="background-color:indigo" disabled>Confirm and Pay</button>
           </div>
       </div>
       </div>
   </div>
</form>
   {{-- end modal --}}
   <!-- Modal -->
<div class="modal fade" id="TermsAndConditionModal" tabindex="-1" role="dialog" aria-labelledby="TermsAndConditionModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
      <div class="modal-content bg-info">    
        <div class="modal-body text-center">
            <h1 class="text-center">Terms and Condition </h1><hr>
            50% downpayment, 50% full payment upon arrival<br>
            No refund
  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn text-white acceptTerm bg-success" data-dismiss="modal">Accept</button>
        </div>
      </div>
    </div>
  </div>
  