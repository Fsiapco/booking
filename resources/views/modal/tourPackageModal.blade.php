<div class="modal fade" id="modalTourBook" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <form action="{{ route('reservation.store') }}" method="POST" id="TourPakcageForm" >
        @csrf

        
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Package Details</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    {{-- {{ Form::text('dates', '', ['id' => 'booking_dates']) }} --}}
                   {{ Form::hidden('room_detail_id', '', ['id' => 'detailsId']) }}   
                   {{ Form::hidden('packageId', '', ['id'=>'packageId']) }}
                
               
                   {{-- User Details --}}
                
                   {{ Form::hidden('special_request', '', ['id'=>'formSpecialRequest']) }}
                   {{ Form::hidden('night', '', ['class'=>'nights']) }}
                   {{ Form::hidden('total_cost', '', ['class'=>'Totalprice'])  }}
                    {{ form::hidden('priceNight','',['id'=>'packagePrice']) }}

                <div class="col-md-5">
                    <div class="row">
                        <div class="card mx-auto justify-centent-center w-75">
                            <div class="">
                                <div><img src="" class = "image" style="height:40vh"></div>
                            
                            </div>
                            <div class="card-body">
                                <h5 class="text-center text-capitalize" id="packageName"></h5>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                        <h5>Package Description:</h5>
                        <p class="text-justify px-5" id="packageDetails"></p>
                        </div>
                    </div>
                    <div class="text-center">
                        <label for="">Package Inclusion</label>
                    </div>  

                    <div style="display: block;position: relative;height: 150px;overflow: auto;">
                        <table class="table table-bordered table-sm text-center" >
                            <thead class="bg-primary">
                                <tr>
                                    <th>Name</th>
                                    <th>Price</th>
                                
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td scope="row"> <div id="pr_result"></div></td>
                                    <td><div id="price"></div></td>
                                </tr>
                            </tbody>
                        </table>                  
                    </div>
                    <hr>

                    <input type="hidden" id="packagePrice" value="">
                    
                    <label for="">Price : <span id="price" >0</span></label>
                </div>
                    
                    <div class="col-md-7 border-left">
                        <div class="row justify-content-center">
                            <img src="../img/UI/sampleLogo.png" class="w-25" alt="">    
                        </div>
                        <div class="row">
                            <h4 class="mt-3 p-2 text-center bg-dark  mx-auto w-50 border rounded-lg">Guest Registration Form</h4>
                        </div>
                
                            <div class="form-group">
                                <div class="row py-2">
                                <label  class="col-sm-3 col-form-label">LOCATION:</label>
                              
                                    <div class="col-md-4 m-0  form-check form-check-inline">
                                        <input class="form-check-input LargeCheckBox" type="radio" name="packageBranch" id="CheckboxBeach1" value="1">
                                        <label class="form-check-label" for="CheckboxBeach">Garden Resort</label>
                                    </div>
                                    <div class="col-md-4 m-0 form-check form-check-inline">
                                        <input class="form-check-input LargeCheckBox" type="radio" name="packageBranch" id="CheckboxBeach" value="2">
                                        <label class="form-check-label" for="CheckboxBeach">Beach Resort</label>
                                    </div>
                                </div>

                                
                            </div>
                                <input type="hidden" name="" id="RoomId" >
                                
                            <div class="form-group avail"  >
                                <div class="row py-2" >
                                    <label  class="col-sm-3 col-form-label  ">Availabe Room:</label>
                                            <div class="form-group  w-75 pr-3" >
                                                <select id="availableRoom" name="SelectedRoom" class="form-control">
                                                    <option>Select Branch</option>
                                                 </select>
                                             
                                            </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-8">
                                        <label class="col-form-label">DATE:</label>
                                        
                                        <input id="PackageDate" type="text" class="form-control  bg-white "  name="dates" readonly>
                                    </div>
    
                                    <div class="form-group col-md-4">
                                        <label for="NoNights" class="col-form-label">NO. OF NIGHT(S)</label>
                                        <input  type="text" class="form-control bg-white nights" readonly>
                                    </div>
                                </div>
                                
            
                                

                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="ClientName" class="col-form-label">First Name:</label>
                                        {{ Form::text('firstname', '', ['id'=>'formFirstName','class' => 'form-control']) }}
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="ClientName" class="col-form-label">Last Name:</label>
                                        {{ Form::text('lastname', '', ['id'=>'formLastName','class' => 'form-control']) }}
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="ClientName" class="col-form-label">Email:</label>
                                        {{ Form::text('email', '', ['id'=>'formEmail','class' => 'form-control']) }}
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="ClientCelNo" class="col-form-label">CELLULAR NO.:</label>
                                        {{ Form::text('contact_number', '', ['id'=>'formContactNumber','class' => 'form-control']) }}
                                    </div>
    
                                    <div class="form-group col-md-6">
                                        <label for="ClientNatinality" class="col-form-label">NATIONALITY:</label>
                                        {{ Form::text('nationality', '', ['id'=>'formNationality','class' => 'form-control']) }}
                                    </div>
                                </div>
    
                                <div class="row py-2 mx-auto">
                                    <div class="form-group col-md-6">
                                            <label  class=" col-form-label">Payment Type:</label>
                                            {{ Form::select('paymentMethod', ['Full Payment' => 'Full Payment', 'Partial Payment' => 'Partial Payment'], 'S',['class' => 'form-control paymenttype']) }}
                                           
                                    </div>
    
                                    <div class="form-group col-md-6">
                                        <label for="ClientNatinality" class="col-form-label">Total Price:</label>
                                        <input type="text" class="form-control Totalprice"  readonly >
                                    </div>
                                </div>



                                <input type="hidden" class="form-control" id="packageArrival" >
                                <input type="hidden" class="form-control" id="packagecheck_out" >

                                <input type="checkbox" id="terms">  <span class="text-bold">I hereby read the term and condition</span> 
                            </div>
                                                      
                  
                    </div>
                    
                </div>
            </div>
            <div class="modal-footer Tourbtn" style="display:none">
                <button type="button" class="btn btn-primary packageBook" disabled>Book Now</button>
            </div>
         
        </div>
    </form>
    </div>
</div>