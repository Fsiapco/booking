<!-- modal use for dynamic -->
  <!-- Garden View Room Modal -->
  <div class="modal fade " id="gardenViewRoom" tabindex="-1"  role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg" role="document" style="-webkit-box-shadow: -1px 2px 36px 4px rgba(222,98,222,0.49);
    -moz-box-shadow: -1px 2px 36px 4px rgba(222,98,222,0.49);
    box-shadow: -1px 2px 36px 4px rgba(222,98,222,0.49);">
      {{-- start of form --}}

        <div class="modal-content w-100">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">name</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body mb-5">
        <form action="{{ route('reservation.store') }}" method="POST" id="gardenRoom">
          @csrf
            <div class="px-5">
              
            <img style="height:400px;" class="w-100 image" src="" alt="">
              <h3 class="mt-2 text-bold">Room Details</h3>
              <p style="font-size:16px;">A very good place to be.</p>
           
        
                <div style="font-size:18px;">
                <i class="fa fa-home" aria-hidden="true"></i><span class="text-bold"> Room No: #</span><span id="room_no"> </span><br>
                </div>
          
              <div style="font-size:18px;">
              <i class="fa fa-bed" aria-hidden="true"></i><span class="text-bold">  No of Bed: #</span>
                    <ul>
                        <li><span id="queenBed"></span> Queen Bed/s</li>
                        <li><span id="singleBed"></span> Single Bed/s </li>  
                    </ul>
              </div>
              <div style="font-size:18px;">
                <i class="fa fa-male" aria-hidden="true"></i><span class="text-bold"> Max No. of Guest/s: </span><span id="guest"></span><br>
              </div>

              <div style="font-size:18px;">
                <i class="fa fa-smile-o" aria-hidden="true"></i><span class="text-bold"> Inclusion:</span>
                <span id="amenities"></span>
                <br>
              </div>
              <div style="font-size:18px;">
              <i class="fas fa-money-bill-alt    "></i><span class="text-bold"> Price/Nights: </span><span id="price"></span> Php<br>
             
              </div>
              <div style="font-size:18px;">
              <i class="fa fa-info-circle" aria-hidden="true"></i></i><span class="text-bold"> Room Description: </span><span id="description"></span><br>
              </div>
            </div>
            <br>
      
               {{-- hidden form input --}}
               {{ Form::hidden('total_cost','', ['id'=>'roomPrice','class' => 'form-control '])  }}
               {{ Form::hidden('room_detail_id','', ['id'=>'room_details_id','class' => 'form-control'])  }}
               {{ Form::hidden('room_id','', ['id'=>'roomID','class' => 'form-control'])  }}
              {{ form::hidden('roomPrice','', ['id'=> 'priceperNight','class'=>'form-control']) }}
           
              <div class="border border-dark p-2 " > 

                <p class="h4 m-4 text-center text-bold border border-dark bg-dark ">Reservation Details</p>
                      <div class="form-row">
                        <div class="col-8">
                          <label for="">Check In - Check Out</label>
                          <input type="text" class="form-control dates" id="roomDate"  name="dates">
                        </div>
                        <div class="col-4">
                          <label for="">No. of night</label>
                          <input type="text" class="form-control nights" name="night"  readonly>
                        </div>
                      </div>
                  
                      <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="ClientName" class="col-form-label">First Name:</label>
                            {{ Form::text('firstname', '', ['id'=>'formFirstName','class' => 'form-control']) }}
                        </div>
                        <div class="form-group col-md-4">
                            <label for="ClientName" class="col-form-label">Last Name:</label>
                            {{ Form::text('lastname', '', ['id'=>'formLastName','class' => 'form-control']) }}
                        </div>
                        <div class="form-group col-md-4">
                            <label for="ClientName" class="col-form-label">Email:</label>
                            {{ Form::text('email', '', ['id'=>'formEmail','class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="ClientCelNo" class="col-form-label">CELLULAR NO.:</label>
                            {{ Form::text('contact_number', '', ['id'=>'formContactNumber','class' => 'form-control']) }}
                        </div>
        
                        <div class="form-group col-md-6">
                            <label for="ClientNatinality" class="col-form-label">NATIONALITY:</label>
                            {{ Form::text('nationality', '', ['id'=>'formNationality','class' => 'form-control']) }}
                        </div>
                    </div>
        
                    <div class="row py-2 mx-auto">
                        <div class="form-group col-md-6">
                                <label  class=" col-form-label">Payment Type:</label>
                                {{ Form::select('paymentMethod', ['Full Payment' => 'Full Payment', 'Partial Payment' => 'Partial Payment'], 'S',['class' => 'form-control paymenttype']) }}
                              
                        </div>
        
                        <div class="form-group col-md-6">
                            <label for="ClientNatinality" class="col-form-label">Subtotal Price:</label>
                            <input type="text" class="form-control Totalprice"  id="cost" readonly >
                        </div>
                    </div>

                    <div class="form-group md-form ">
                                    <label for="request" class="font-weight-bold text-center   w-100">Special Request:</label>
                                    {{ Form::hidden('specialrequest', '', ['class' => 'form-control text-center  font-weight-bold','id'=>'requestValue']) }}
                                    <input type="hidden" id="requestTotal" class="SubTotal">
                                    <input type="hidden" id="counter" class="">
                                      <div>
                                          <span>Note!  </span>
                                      </div>
                                  
                                    <div class="custom-checkbox">
                                        <div class="row text-bold pl-4">
                                            <div class="col-6 ">
                                                Name
                                            </div>
                                            <div class="col-6">
                                               Price
                                            </div>
                                        </div>
                                        @foreach ($guestRequest as $request)
                                            <div class="form-check">
                                                <div class="row">
                                                    <div class="col-6">
                                                         <label>
                                                            <input type="checkbox" name="request" id="guestRequest" class="request" :data-price="{{ $request->price }}" value="{{ $request->name }}"> <span class="label-text"> {{ $request->name  }} </span> 
                                                        </label>
                                                    </div>
                                                    <div class="col-6">
                                                        <span class="text-success"> ₱{{ $request->price }}</span>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                
                                <input type="checkbox" name="" id="terms"> I hereby agree to the <button type="button"  data-toggle="modal" data-target="#TermsAndConditionModal">Terms and Condition</a> 
                                <br>
                                <div class="w-100 ">
                                   
                                </div>
                
              </div>
           
            <div class="text-right mt-3">
              <button type="button" class="btn btn-success p-3 BookNow  w-100" disabled ><i class="fab fa-paypal fa-2x"></i> <span class="mx-auto">aid </span>  <span id="totalToPaid" class="text-bold"></span> Php</button>
            </div>
            
        </form>
          </div>
       
         
        </div>
 
    </div>
  </div>
  
<!-- Modal -->
<div class="modal fade" id="TermsAndConditionModal" tabindex="-1" role="dialog" aria-labelledby="TermsAndConditionModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered " role="document">
    <div class="modal-content bg-info">    
      <div class="modal-body text-center">
          <h1 class="text-center">Terms and Condition </h1><hr>
          50% downpayment, 50% full payment upon arrival<br>
          No refund

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn text-white acceptTerm bg-success" data-dismiss="modal">Accept</button>
      </div>
    </div>
  </div>
</div>
