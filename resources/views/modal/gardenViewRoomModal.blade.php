  <!-- Modal -->
  <div class="modal fade" id="gardenViewRoomModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Garden View Room</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="container-fluid mt-5">
            <div class="row justify-content-center mt-5 mb-5">
                <div class="col-md-6 col-sm-9 col-xs-1 p-2 picture-padding" style="box-shadow: 0px 0px 3px #6A1F85; ">

                        @if ($rooms->room_images->count() > 0 )
                            <div class="slider slider-for w-100">
                                    @foreach ($rooms->room_images as $roomImages)
                                    <div class="justify-content-center">
                                        <h3> <img src="{{ asset('/img/rooms/'.$roomImages->image) }}" class="moreImages"  ></h3>
                                    </div>
                                    @endforeach

                            </div>
                            <div class="slider slider-nav">
                                @foreach ($rooms->room_images as $roomImages)
                                <div class="image-container-slider">
                                    <img src="{{ asset('/img/rooms/'.$roomImages->image) }}" class="img-selector">
                                </div>
                                @endforeach
                            </div>
                        @else
                        <div class="p-3">
                                <img src="{{ asset('/img/rooms/'.$rooms->photo) }}" class="img-fluid moreImages" >
                        </div>


                        @endif


                    @php
                        $roomDetails = $rooms->room_details->first();
                    @endphp
                    <div class="mt-3 roomDetails  " data-aos="zoom-in"
                    data-aos-duration="3000">
                        <div class="p-0 m-3" style="background-image: linear-gradient(to left, #410064 , violet);">
                                <h3 class="text-center font-weight-bold text-uppercase roomName"><span class="text-success">{{ $rooms->type}}</span> </h3>
                        </div>
                        <div class="text-center" style="line-height:1px;">
                                <label for="" ><span  class="titleDetails"> <h1>Room Details</h1></span>
                                    <span class="text-italic">A very good place to be.</span>
                                </label>
                        </div>
                        <div class="details-body mt-4 pl-4">
                                <div class="row">
                                    <div class="col-md-6">
                                            <i class="fa fa-bed" aria-hidden="true"></i>  <label for="">Room No. : #</label>
                                    <span> {{$roomDetails->room_no}}</span>
                                    </div>
                                    <div class="col-md-6">
                                            <i class="fa fa-male" aria-hidden="true"></i> <label for="">Max No. of Guest : </label>
                                                <span>{{$rooms->guests}}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <i class="fa fa-smile-o" aria-hidden="true"></i> <label for="">Inclusion : </label>
                                        <ul class="inclusion">
                                            @foreach($rooms->room_amenities as $inclusion)
                                                <li>&nbsp;{{$inclusion->amenities}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-sm-6" >
                                            <div>
                                                    <i class="fas fa-money-bill-wave    "></i> <label for="">Price/Nights : </label> <span class="text-success">&#8369;{{$rooms->price}}</span>
                                            </div>
                                    </div>
                                </div>
                                <div>
                                <p style="font-size:15px; line-height:20px; text-align:justify; text-indent:50px;">
                                    {{$rooms->description}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-9  mt-2" id="receiptContainer"   data-aos="fade-left"
                data-aos-anchor="#example-anchor"
                data-aos-offset="500"
                data-aos-duration="500">
                    <div style="box-shadow: 0px 0px 5px gray; border: 1px solid black;   m-4 p-0 bg-white" >
                        <div class="p-3" style="border-bottom: 1px dashed black;">
                            <h3 class="text-center" style="color:black;font-weight:bold">Reservation Details</h3>
                        </div>
                        <div class="p-3">
                                <form class="" id="bookingForm" autocomplete="off">


                                    <div class="form-group md-form">
                                        <label for="dates" class="font-weight-bold">Check In - Check Out</label>
                                        {{ Form::text('dates', $checkIn . ' - ' . $checkOut , ['id'=>'dates', 'class' => 'form-control text-center font-weight-bold', 'placeholder' => 'Check In - Check Out', 'autocomplete'=>'off','style'=>'color:#440266;font-size:18px;']) }}
                                        {{ Form::hidden('room_id', $rooms->room_details->first()->id, ['id' => 'room_id']) }}
                                        {{ Form::hidden('room_price', $rooms->price, ['id'=>'room_price']) }}
                                        {{ Form::hidden('total_cost', old('total_cost'), ['id'=>'cost']) }}

                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 form-group md-form">
                                            <label for="name" class="font-weight-bold">First Name:</label>
                                            {{ Form::text('fname', '', ['class' => 'form-control text-center font-weight-bold','id'=>'fname']) }}
                                        </div>
                                        <div class="col-md-6 form-group md-form">
                                            <label for="name" class="font-weight-bold">Last Name:</label>
                                            {{ Form::text('name', '', ['class' => 'form-control text-center font-weight-bold','id'=>'lname']) }}
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group md-form">
                                                <label for="contact" class="font-weight-bold">Contact #:</label>
                                                {{ Form::text('contact', '09061237968', ['class' => 'form-control text-center  font-weight-bold','id'=>'contact']) }}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group md-form">
                                                    <label for="Nationality" class="font-weight-bold">Nationality:</label>
                                                    {{ Form::text('nation', '', ['class' => 'form-control text-center  font-weight-bold','id'=>'Nationality']) }}
                                            </div>
                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group md-form">
                                                    <label for="email" class="font-weight-bold">Email:</label>
                                                    {{ Form::text('email', '', ['class' => 'form-control text-center  font-weight-bold','id'=>'email']) }}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group md-form">
                                                    <label for="conemail" class="font-weight-bold">Confirm Email:</label>
                                                    {{ Form::text('conEmail', '', ['class' => 'form-control text-center  font-weight-bold','id'=>'confirmEmail']) }}
                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-group md-form">
                                        <label for="request" class="font-weight-bold">Special Request:</label>
                                        {{ Form::hidden('request', '', ['class' => 'form-control text-center  font-weight-bold','id'=>'requestValue']) }}
                                        <input type="hidden" id="requestTotal">

                                        <div class="custom-checkbox">
                                            <div class="row text-bold">
                                                <div class="col-6 text-center">
                                                    Name
                                                </div>
                                                <div class="col-6">
                                                   Price
                                                </div>
                                            </div>
                                            @foreach ($guestRequest as $request)
                                                <div class="form-check">
                                                    <div class="row">
                                                        <div class="col-6">
                                                             <label>
                                                                <input type="checkbox" name="request" class="request" :data-price="{{ $request->price }}" value="{{ $request->name }}"> <span class="label-text"> {{ $request->name  }} </span>
                                                            </label>
                                                        </div>
                                                        <div class="col-6">
                                                            <span class="text-success"> ₱{{ $request->price }}</span>
                                                        </div>
                                                    </div>

                                                </div>
                                            @endforeach
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                {{ Form::hidden('payment', '', ['class' => 'form-control text-center  font-weight-bold','id'=>'paymentofMethod']) }}
                                                <label for="">  Mode of Payment :</label>
                                                <select class="form-control" name="paymentMethod" id="paymentMethod">
                                                  <option value="Full Payment">Full Payment</option>
                                                  <option value="Partial Payment">Partial Payment</option>
                                                </select>
                                              </div>
                                        </div>

                                    </div>
                                    <p>
                                            <span class="font-weight-bold">Nights: <span class = "noOfNights"></span></span>
                                    </p>
                                    <h3>
                                        <span class="font-weight-bold">Total Cost:</span>
                                        <span class="float-right font-weight-bold" style="color:#61B361" id = "totalCost"></span>
                                        <span class="float-right font-weight-bold" style="color:#61B361">&#8369;</span>
                                    </h3>
                                    {{ Form::submit('Book Now', ['class'=>'btn form-control btnBook text-uppercase font-weight-bold m-0','style'=>'background-color:#410064;color:white; letter-spacing:1px; font-size: 20px;']) }}

                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>
