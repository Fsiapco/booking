<div class="modal fade" id="Ardent-Hot-Spring" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-10">
                        <h5 class="modal-title" id="exampleModalLabel">Ardent Hot Spring</h5>
                    </div>
                    <div class="col-2">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-7 slickTourPackage-for mx-auto px-2">
                        <img src="{{asset('img/locale-attraction/ardent-hot-spring.jpg')}}" class="w-100" alt="">
                        <img src="{{asset('img/locale-attraction/ardent-hot-spring.jpg')}}" class="w-100" alt="">
                        <img src="{{asset('img/locale-attraction/ardent-hot-spring.jpg')}}" class="w-100" alt="">
                        
                    </div>
                 
                </div>
                <div class="row">
                <div class="col-md-7 slickTourPackage-nav mx-auto px-2">
                        <img src="{{asset('img/locale-attraction/ardent-hot-spring.jpg')}}" class="w-100" alt="">
                        <img src="{{asset('img/locale-attraction/ardent-hot-spring.jpg')}}" class="w-100" alt="">
                        <img src="{{asset('img/locale-attraction/ardent-hot-spring.jpg')}}" class="w-100" alt="">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>