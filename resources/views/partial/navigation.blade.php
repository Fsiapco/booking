<div class="container navCont" id="mainNav">
<nav class="navbar navbar-expand-lg navbar-light pt-4 pb-4 bg-transparent">

  <a class="navbar-brand" href="/"><img class="logo" src="{{ asset('../img/UI/sampleLogo.png') }}" style="width:150px;" alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
      <span><i class="fa fa-bars "></i></span>
    </button>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">

    <ul class="navbar-nav ml-auto mt-lg-5">
      <li class="nav-item mr-3">
        <a class="nav-link" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item mr-3">
            <div class="btn-group dropdown">
                    <a  class="dropdown-toggle nav-link rooms" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                         Services
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                      <a href="/tour-Package"><button class="dropdown-item nav-link rooms" type="button">Tour Package</button></a>
                      <a href="/van-transport"><button class="dropdown-item nav-link rooms" type="button">Van Transport</button></a>
                      <a href="/catering-Service"><button class="dropdown-item nav-link rooms" type="button">Catering</button></a>
                    </div>
            </div>
      </li>


      <li class="nav-item mr-3">
            <div class="btn-group dropdown">
                    <a  class="dropdown-toggle nav-link rooms" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                         Accommodation
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                      <a href="/garden-resort"><button class="dropdown-item nav-link rooms" type="button">Garden Resort</button></a>
                      <a href="/beach-resort"><button class="dropdown-item nav-link rooms" type="button">Beach Resort</button></a>
                    </div>
                  </div>
      </li>


      <li class="nav-item mr-3">
        <div class="btn-group dropdown">
                <a  class="dropdown-toggle nav-link rooms" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Quotation
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                  <a href="/functions-quotation"><button class="dropdown-item nav-link rooms" type="button">Request for Quotation</button></a>

                </div>
        </div>
      </li>
      <li class="nav-item mr-3">
            <div class="btn-group dropdown">
                    <a  class="dropdown-toggle nav-link rooms" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Locale
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                      <a href="locale-attraction"><button class="dropdown-item nav-link rooms" type="button">Attraction</button></a>
                      <a href="locale-delicacies"><button class="dropdown-item nav-link rooms" type="button">Delicacies</button></a>
                      <a href="locale-events"><button class="dropdown-item nav-link rooms" type="button">Events</button></a>
                    </div>
            </div>
      </li>
      <li class="nav-item mr-3">
            <a class="nav-link " href="#booking" id="navBooking">Booking <span class="sr-only">(current)</span></a>
      </li>

      <li class="nav-item mr-3">
        <a class="nav-link" href="#contact" id="navContact">Contact <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item mr-3">
        <a class="nav-link " href="/#booking" id="navBookings">Booking <span class="sr-only">(current)</span></a>
      </li>

      <li class="nav-item mr-3">
        <a class="nav-link" href="/#contact" id="navContacts">Contact <span class="sr-only">(current)</span></a>
      </li>
    </ul>

  </div>
</nav>
</div>
