<!-- Footer -->
<footer class="page-footer font-small unique-color-dark bg-white">

    <div style="background-color: #6351ce;color:white;">
      <div class="container">

        <!-- Grid row-->
        <div class="row py-4 d-flex align-items-center">

          <!-- Grid column -->
          <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
            <h6 class="mb-0">Get connected with us on social networks!</h6>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-md-6 col-lg-7 text-center text-md-right">

            <!-- Facebook -->
            <a class="fb-ic linkformat" href="https://www.facebook.com/havendwellbeachresort/" target="_blank">
                Garden Resort <i class="fab fa-facebook-f white-text mr-4"> </i>
            </a>

             <!-- Facebook -->
             <a class="fb-ic linkformat" href="https://www.facebook.com/havendwellbeachresort/" target="_blank">
                Beach Resort <i class="fab fa-facebook-f white-text mr-4"> </i>
            </a>

            <!--Instagram-->
            <a class="ins-ic linkformat" href="https://www.instagram.com/havendwellresort/" target="_blank">
              Havendwell Resort<i class="fab fa-instagram white-text"> </i>
            </a>

          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row-->

      </div>
    </div>

    <!-- Footer Links -->
    <div class="container text-center text-md-left mt-5">

      <!-- Grid row -->
      <div class="row mt-3">

        <!-- Grid column -->
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">

          <!-- Content -->
          <h6 class="text-uppercase font-weight-bold">Havendwell Resort</h6>
          {{-- <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;"> --}}
          <img src="{{ asset('img/UI/sampleLogo.png') }}" alt="" style="width:200px;" srcset="">

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

          <!-- Links -->
          <h6 class="text-uppercase font-weight-bold">List of Rooms</h6>
          <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        
     

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">

          <!-- Links -->
          <h6 class="text-uppercase font-weight-bold">Navigation</h6>
          <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
          <p>
            <a href="#" class="footerLink">Home</a>
          </p>
          <p>
            <a href="#!" class="footerLink">Available Rooms</a>
          </p>
@section('style')

@endsection
          <p>
            <a href="#events" class="footerLink">Events</a>
          </p>

          <p>
            <a href="#about-us" class="footerLink">About</a>
          </p>
          <p>
            <a href="#contact-us" class="footerLink">Contact</a>
          </p>


        </div>

         <!-- Grid column -->
         <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
            <!-- Links -->
            <h6 class="text-uppercase font-weight-bold">Contact</h6>
            <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
            <div class="row">
              <div class="col-md-12">
                <h4 class="text-bold footerLink">Beach Resort</h4>
                <p><i class="fas fa-home mr-3"></i>Looc, Yumbing, Mambajao, Camiguin</p>
                
                <p><i class="fas fa-envelope mr-3"></i> havendwellresort@gmail.com</p>
                
                <p><i class="fas fa-phone mr-3"></i> 0917-122-0221</p>
              </div>
              
            </div>
            
            <div class="row m-3">
            <div class="col-6 border-top border-gray"></div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <h4 class="text-bold footerLink">Garden Resort</h4>

                <p><i class="fas fa-home mr-3"></i>Rizal St., Upper Poblacion, Mambajao, Camiguin</p>

                <p><i class="fas fa-envelope mr-3"></i> havendwellresort@gmail.com</p>

                <p><i class="fas fa-phone mr-3"></i> 0917-122-0221</p>
              </div>
            </div>
        </div>
<!-- Grid column -->

      </div>
      <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© {{date('Y')}} Copyright:
      <a href="https://naotech.com.ph/"> Naotech.com.ph</a>
    </div>
    <!-- Copyright -->

  </footer>
  <!-- Footer -->
