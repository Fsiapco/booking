@extends('layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <img src="{{asset('img/UI/tour-package.jpg')}}" class="w-100" alt="">
        </div>
        <div class="row ">
            <div class="col-md-6 offset-md-1 py-5">
                <table class="table col-12 table-bordered text-center table-striped ">
                    <thead class="thead-dark">
                        <tr>
                            <th>From / To</th>
                            <th>Per Pick Up / Drop Off</th>
                        </tr>
                    </thead>
                    <tbody class=""> 
                        <tr class="text-center">
                            <td>Airport</td>
                            <td>₱500</td>
                        </tr>
                        <tr class="text-center">
                            <td>Balbagon Sea Port</td>
                            <td>₱500</td>
                        </tr>
                        <tr class="text-center">
                            <td>Benoni Sea Port</td>
                            <td>₱700</td>
                        </tr>
                    </tbody>
                </table>

        </div>
        <div class="col-md-4 pt-5">
            <table class="table col-12 table-bordered text-center table-striped ">
                <thead class="thead-dark">
                    <tr>
                        <th>Camiguin Tour</th>
                    </tr>
                </thead>
                <tbody class=""> 
                    <tr class="text-center">
                        <td>₱3,500/day, good for 12 pax</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="row" style="height:75vh;background-image:url('img/UI/new8-beach.png');background-repeat:no-repeat;background-size:cover;background-position:center;background-attachment:fixed">
            <div class="col vw-100 px-0">
                <img src="{{asset('img/UI/slogan.png')}}" class="py-5 w-50 float-right" alt="test">
               
            </div>
        </div>
    </div>
@endsection