@extends('layouts.master')

@section('style')
    <style scoped> 
        .error{
            color: red;
            font-weight: :bold;
        }
    </style>
@endsection
@section('content')

    <div class="container-fluid">
        <div class="row">
            <img src="{{asset('img/UI/TourPackage.jpg')}}" class="w-100" alt="">
        </div>
    </div>

    <div class="container mx-auto">
        <div class="row py-3">
        @forelse ($package as $data)
            <div class="col-md-4 mx-auto">
                <div class="card mx-auto" style="width: 18rem;">
                    <img src="{{asset('img/package/'.$data->img)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">{{ $data->packagename }}</h5>
                        <p class="card-text">{{ $data->details }}</p>
                    <a href="" class="btn btn-primary float-right packageModal" type="button"   package-id="{{$data->package_id}}" data-toggle="modal"  data-target="#modalTourBook">Book Tour</a>
                    </div>

                    
                  
                </div>
            </div>
        @empty
        <div class="jumbotron w-100 text-center" style="height:">
            <div class="col-12">
                <div class="card">
                  <div class="card-header ">
                    <h3 class="card-title ">Tour Package</h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body table-responsive p-0">
                    <table class="table table-bordered table-striped">
                      <thead class="bg-dark">
                        <tr>
                          <th></th>
                          <th colspan="2">Beach</th>
                          <th colspan="2">Garden</th>
                        </tr>
                      </thead>
                      <tbody class="">
                        <tr>
                          <td>No. of Pax</td>
                          <td>Van</td>
                          <td>Multicab</td>
                          <td>Van</td>
                          <td>Multicab</td>                       
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>₱ 7,025.00</td>
                            <td>₱ 5,325.00</td>
                            <td>₱ 7,025.00</td>
                            <td>₱ 5,325.00</td> 
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>₱ 7,316.67</td>
                            <td>₱ 4,183.00</td>
                            <td>₱ 5,316.67</td>
                            <td>₱ 4,183.00</td> 
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>₱ 5,012.50</td>
                            <td>₱ 4,162.50</td>
                            <td>₱ 4,512.50</td>
                            <td>₱ 3,662.50</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>₱ 4,780.00</td>
                            <td>₱ 4,100.00</td>
                            <td>₱ 3,800.00</td>
                            <td>₱ 3,120.00</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>₱ 4,283.33</td>
                            <td>₱ 3,716.67</td>
                            <td>₱ 3,250.00</td>
                            <td>₱ 2,683.33</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>₱ 3,814.29</td>
                            <td>₱ 3,328.57</td>
                            <td>₱ 3,100.00</td>
                            <td>₱ 2,614.29</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>₱ 3,537.50</td>
                            <td>₱ 3,112.50</td>
                            <td>₱ 2,937.50</td>
                            <td>₱ 2,512.50</td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>₱ 3,461.11</td>
                            <td>₱ 3,083.33</td>
                            <td>₱ 2,861.11</td>
                            <td>₱ 2,483.33</td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>₱ 3,355.00</td>
                            <td>₱ 3,015.00</td>
                            <td>₱ 2,755.00</td>
                            <td>₱ 2,415.00</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->
              </div>
        </div>
        @endforelse
        </div>

          <!-- modal -->
         
    </div>

    @include('modal.tourPackageModal')
 
@endsection
@section('script')
<script src="{{ asset('js/tourPackageValidation.js') }}"></script>
    <script>
        $(document).ready(function(){
           
            $('#modalTourBook').on('hide.bs.modal', function (e) {
            location.reload();
             })

            $('.packageModal').click(function(){
                $('#availableRoom').html('');
                $('#availableRoom').children().remove().end().append('<option selected value="">Select Room</option>') ;            
                var id = $(this).attr('package-id');
                $('#packageId').val(id);
                $('.avail').css('display','none');
                $('.LargeCheckBox').prop("checked", false);
                var room = [];
                let headers = new Headers();
                headers.append('Authorization', this.authToken);
                headers.append('Content-Type', 'application/json');

               $(".LargeCheckBox").click(function(){
                    var request = [];
                    var requestSubtotal = 0;
                    var requestValue = [];
                    $('#availableRoom').html('');
                    $('#availableRoom').children().remove().end().append('<option selected value="">Select Room</option>') ;
                    $('#PackageDate').attr('readonly','readonly');
                    $('.avail').css('display','');
                    $('.Tourbtn').css('display','');
             
                        $.each($("input[name='packageBranch']:checked"), function(){
                            var branch_id = $(this).val();
                       
                            $.getJSON('/getAvailableRooms/'+branch_id)
                                .done(function(data) {
                                    $.each( data.room, function(index, value) {
                                            $.each(value.room_details, function(key, value) {
                                                $('#availableRoom')
                                                    .append($("<option></option>")
                                                    .attr("value", value.room_id)
                                                    .attr("detail-id",value.id)
                                                    .text(value.room_no));
                                                    
                                            });

                                        });
                                        
                                        $('#availableRoom').change(function(){
                                            var roomdetailsId = $('option:selected', this).attr('detail-id');
                                            $('#detailsId').val(roomdetailsId);
                                            var roomID = $('#availableRoom').val();
                                          
                                            $.getJSON('/getbookRoom/'+roomID)
                                            .done(function(data) {
                                                var datesArrays = data.dates;
                                                var input = document.getElementById('PackageDate');
                                                var dateArray = [].concat.apply([], datesArrays); 
                                  
                                                var datepicker = new HotelDatepicker(input,{
                                                format: 'YYYY-MM-DD',
                                                minNights: 1,
                                             
                                                showTopbar: false,
                                                disabledDates:dateArray,
                                                onSelectRange: function() {
                                                    var values = datepicker.getValue();
                                                    var nights = datepicker.getNights();
                                                    var dates = values.split(/ - /);
                                                    var packageArrival =  $("#packageArrival").val(dates[0]);
                                                    var packagecheck_out = $("#packagecheck_out").val(dates[1]);
                                                    var price =  $(".Totalprice").val();
                                                    var packagePrice = $('#packagePrice').val();
                                                    var total_price = nights * packagePrice;
                                                    var roomdetailsId = $('#detailsId').val()
                                                    var packageId =  $('#packageId').val();
                                                
                                                    var RoomId = $('#availableRoom').val();

                                                    $('.Totalprice').val(total_price);
                                                    $(".nights").val(nights);
                                                

                                                }
                                                
                                            });
                                            datepicker.open(); 
                                            
                                            });

                                          
                            
                                            });
                                });
                        });
                });

             

           

                $('.paymenttype').change(function(){
                  
                    if($(this).val() == 'Partial Payment'){
                    var partial =  $('.Totalprice').val() / 2;
                        $('.Totalprice').val(partial);
                    }else{
                        var full =  $('.Totalprice').val() * 2;
                        $('.Totalprice').val(full);
                    }
                
                });

            //fetch Package Details
            $("#pr_result").html('');
            $("#price").html('');
            $.ajax({
                url: 'tourPackage-Modal/'+ id,
                method: 'GET',
                dataType : 'json',
                headers: {  'Access-Control-Allow-Origin': '*' },
                data:'_token = <?php echo csrf_token() ?>',
                success : function(data){
                   
              
                    $('.image').attr('src', '/img/package/'+data['img']);
                    $('#packageName').html(data['packagename']);
                    $('#packageDetails').html(data['details']);
                    jQuery.each(data.package_details, function(index, item) {
                        $("#pr_result").append(item.Itemname + '<br>');
                        $("#price").append(item.price + '<br>');
                    });
                    $('#packagePrice').val(data['total_price']);
                    $('span#price').html(data['total_price']);
                    $('#modalTourBook').modal('show');
                   
                }
            });
               
            });
            
            //enabled button if check 
            $('#terms').click(function(){
                if($(this).prop("checked") == true){
                    $('.packageBook').removeAttr('disabled');
                }
                else if($(this).prop("checked") == false){
                    $('.packageBook').attr('disabled','disabled');
                }
            });

            $('.packageBook').click(function(e){
        
                if($('#TourPakcageForm').valid()){
                    $('.packageBook').attr('type','submit');
                  
                }
            });
        

        })
    </script>
@endsection
