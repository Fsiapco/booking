@extends('layouts.master')
@section('style')
        <style scoped>
                tr:nth-child(even) td {
                        background: #FFFFFF;
                }
                tr:nth-child(odd) td {
                        background: #343A40;        
                }
                tr:nth-child(odd) td #booking {
                        color:white !important;
                }
                tr:nth-child(even) td #booking {
                        color:#343A40 !important;
                }
                tr:nth-child(even) #text-attraction{
                        color: white !important;
                }
                tr:nth-child(odd) #text-attraction{
                        color: black !important;
                        background: #FFFFFF !important;
                }
                
        </style>
@endsection
@section('content')

<div class="container-fluid">
        <div class="row">
                <img src="../img/UI/local-attraction-header.jpg" class="w-100" alt="">
        </div>
</div>

<table >
        @for($x = 0 ; $x < 10 ;  $x++ )
        <tr class="row p-4 my-4 bg-gray"  data-aos="fade-up" data-aos-duration="4000" style="display:flex; width: 80%; margin-left:10%; margin-right:10%;">
                <td class="col-md-4" style="height:auto">          
                        <div class="text-center p-4">
                                <h1 style="font-weight:bold;color:gray"   id="booking">ARDENT HOT SPRING</h1>
                                <div class="card text-white">
                                <img class="card-img" style="height:200px;"src="" alt="Card image">
                        </div>
                </td>
                <td class="col-md-8 bg-dark locale-attraction-text py-5" id="text-attraction">
                        <p>
                                There are number of thermal pools that can be found on the island and the Ardent Hot Spring is the most popular. It’s one of the Tourist Spots you shouldn’t miss. 
                        </p>
                        <p>
                                Entrance Fee: Php 30.00   
                        </p>
                        <p>
                                Photo Credit: <a href="https://jonnymelon.com/ardent-hot-springs/">https://jonnymelon.com/ardent-hot-springs/</a> 
                        </p>
                </td>
        </tr>
        @endfor
</table>



@endsection

@section('script')
    <script>
            	$('.SeeMore2').click(function(){
		var $this = $(this);
		$this.toggleClass('SeeMore2');
		if($this.hasClass('SeeMore2')){
			$this.text('See More');			
		} else {
			$this.text('See Less');
		}
	});
        AOS.init();
    </script>
@endsection