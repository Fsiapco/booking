@extends('layouts.master')
@section('content')

    <div class="container-fluid">
        <div class="row">
            <img src="{{asset('img/UI/catering.jpg')}}" class="w-100" alt="">
        </div> 
        <br><br><br>
        <div class="row mt-4 justify-content-center">
            <div class="col-12 text-center">
                <h2 class="font-weight-bold">PACKAGE RATE (minimum of 50 pax)</h2>
                <h5>₱ 250 per pax (1 Soup, 1 Main Course, 1 Vegetable, 1 Appetizer or Pasta, 1 Dessert and 1 Round Softdrinks)</h5>
                <h5>₱ 350 per pax (1 Soup, 3 Main Courses, 1 Vegetable, 1 Appetizer or Pasta, 1 Dessert and 1 Round Softdrinks)</h5>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="text-center mt-5 font-weight-bold">SOUP</h4>
                        <table class="table text-center col-12 table-striped table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Beef</th>
                                    <th scope="col">Chicken</th>
                                    <th scope="col">Pork</th>
                                    <th scope="col">Others</th>
                                </tr>
                            </thead>
                            <tbody> 
                                <tr>
                                    <td>Nilaga</td>
                                    <td>Tinola</td>
                                    <td>Sinigang</td>
                                    <td>Cream of Corn</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Surol</td>
                                    <td>Nilaga</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Sotanghon Soup</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <h4 class="text-center mt-5 font-weight-bold">MAIN COURSE</h4>
                        <table class="table col-12 text-center table-bordered table-striped ">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Beef</th>
                                    <th scope="col">Chicken</th>
                                    <th scope="col">Pork</th>
                                    <th scope="col">Fish</th>
                                </tr>
                            </thead>
                            <tbody> 
                                <tr>
                                    <td>Steak</td>
                                    <td>Fried</td>
                                    <td>Steak</td>
                                    <td>Fish Fillet</td>
                                </tr>
                                <tr>
                                    <td>Caldereta</td>
                                    <td>Buttered</td>
                                    <td>Sweet and Sour</td>
                                    <td>Sweet and Sour</td>
                                </tr>
                                <tr>
                                    <td>Beef Brocolli</td>
                                    <td>Cordon Bleu</td>
                                    <td>Adobo</td>
                                    <td></td>
                                </tr>
                                <tr>

                                    <td>Beef Ampalaya</td>
                                    <td>Adobo</td>
                                    <td>BBQ</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>BBQ</td>
                                    <td>Porkchop</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="text-center mt-5 font-weight-bold">VEGGIES</h4>
                        <table class="table col-12 table-bordered table-striped ">
                            <thead class="thead-dark">
                                <tr>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class=""> 
                                <tr class="text-center">
                                    <td>Pinakbet</td>
                                </tr>
                                <tr class="text-center">
                                    <td>Chopseuy</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <h4 class="text-center mt-5 font-weight-bold">APPETIZER OR PASTA</h4>
                        <table class="table col-12 table-bordered text-center table-striped ">
                            <thead class="thead-dark">
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class=""> 
                                <tr class="text-center">
                                    <td>Cucumber Salad</td>
                                    <td>Sotanghon Guisado</td>
                                </tr>
                                <tr class="text-center">
                                    <td>Lettuce Salad</td>
                                    <td>Pancit Canton</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>  

                <div class="row">
                    <div class="col-md-6">
                        <h4 class="text-center mt-5 font-weight-bold">DESSERTS </h4>
                        <table class="table col-12 table-bordered text-center table-striped ">
                            <thead class="thead-dark">
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class=""> 
                                <tr class="text-center">
                                    <td>Fresh Fruits in Season</td>
                                    <td>Tapioca</td>
                                </tr>
                                <tr class="text-center">
                                    <td>Maja Blanca</td>
                                    <td>Macaroons</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6 my-5">
                        <table class="table col-12 table-bordered text-center table-striped ">
                            <thead class="thead-dark">
                                <tr>
                                    <th colspan="2">INCLUSIONS</th>
                                </tr>
                            </thead>
                            <tbody class=""> 
                                <tr class="text-center">
                                    <td>Aircon Venue</td>
                                    <td>Standby Water Dispenser</td>
                                </tr>
                                <tr class="text-center">
                                    <td>Sound System</td>
                                    <td>Standby Generator</td>
                                </tr>
                                <tr class="text-center">
                                    <td>Wide Screen Television</td>
                                    <td>Simple Decoration</td>
                                </tr>
                                <tr class="text-center">
                                    <td>Tables and Chairs</td>
                                    <td>3 hours free (in excess of 3 hours, additional ₱1,500/hr)</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>        
                </div>
            </div>    
        </div>
        
       <div class="container border rounded my-3">
            <h3 class="my-2">We also host venue for event celebration</h3>
            <div class="row my-2 catering-event">
                <div class="col-md-3"><img src="{{asset('img/catering/catering-venue1.jpg')}}" class="w-100"></div>
                <div class="col-md-3"><img src="{{asset('img/catering/catering-venue2.jpg')}}" class="w-100"></div>
                <div class="col-md-3"><img src="{{asset('img/catering/catering-venue3.jpg')}}" class="w-100"></div>
                <div class="col-md-3"><img src="{{asset('img/catering/catering-venue4.jpg')}}" class="w-100"></div>
            </div>
       </div>
    </div>
@endsection

