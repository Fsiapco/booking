<div class="row" style="display:flex;">
        <div class="col-md-12 text-center pt-4">
                <h1 style="font-weight:bold;color:violet"   id="booking">HAVENDWELL ROOMS</h1>
        </div>
        <div class="col-md-4 offset-md-2" id="order-one">

                <div class="p-4 text-center">
                        <div class="card text-white">
                                <img class="card-img" style="height: 290px"src="{{ asset('img/UI/gardenroom.jpg') }}" alt="Card image">
                                <div class="card-img-overlay roomOverlay" style="">
                                        <div class="row justify-content-center pt-2">
                                                <h5 class="card-title">Garden Resort Rooms</h5>
                                        </div>
                                        <div class="row justify-content-center">
                                                <a class="btn" style="background-color: rgb(225, 153, 51);color:white;border:none;" href="garden-resort" role="button">CHECK AVAILABILITY AND PRICES</a>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>

        <div class="col-md-4 " id="order-one">
                <div class="p-4 text-center">
                        <div class="card text-white">
                                <img class="card-img"  src="{{ asset('img/UI/view2.jpg') }}" alt="Card image">
                                <div class="card-img-overlay roomOverlay" style="">
                                        <div class="row justify-content-center pt-2">
                                                <h5 class="card-title">Beach Resort Rooms</h5>
                                        </div>
                                        <div class="row justify-content-center">
                                                <a class="btn" style="background-color: rgb(225, 153, 51);color:white;border:none;" href="beach-resort" role="button">CHECK AVAILABILITY AND PRICES</a>
                                        </div>
                                </div>
                        </div>
                </div>

        </div>
        <div class="col-md-10 offset-md-2 bg-HW_violet havendwell-room-text" id="order-three">
                <p>Remarkable views of nature are there when you wake up in the morning and before you retreat at night. The cultural distinctions are evident in its cozy villas and cottages.
                </p>

                        <p>With different options of quality, acommodations, each travelers needs will certainly be addressed at Havendwell. All villas and cottages have balconies where you can bask and marvel at the beauty around you together with these amenities.
                        </p>
                        <ul class="rooms">
                                @foreach ($amenities as $amenitie)
                                         <li  data-aos="zoom-in" data-aos-offset="100"  data-aos-easing="ease-in-sine">&nbsp;{{ $amenitie->amenities }}</li>
                                @endforeach
                        </ul>
                        <!-- <button style="width:20%;background-color: rgb(225, 153, 51);color:white;border:none;text-transform:uppercase">view all</button> -->
                </div>
        </div>
        <div class="col-md-8 offset-md-2" id="order-four">
                <div class="row">
                        <div class="col bg-HW_violet">
                                <div class="p-4 text-center">
                                        <div class="card text-white">
                                                <img class="card-img" src="{{ asset('img/UI/landing6.jpg') }}" alt="Card image">
                                        </div>
                                </div>
                        </div>
                        <div class="col bg-HW_violet">
                                <div class="p-4 text-center">
                                        <div class="card text-white">
                                                <img class="card-img" src="{{ asset('img/UI/landing1.jpg') }}" alt="Card image">
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</div>



<div class="row">
        <div class="col-md-4" style="background-color: rgb(255,225,194); border-top: solid 5px #4E0D4A;
        border-bottom: solid 5px #4E0D4A;">
                <h3 class="headingText" style="padding-left:50px;text-transform:uppercase;font-weight:bold;font-family: Arial, Helveticam sans-serif; padding-top:15px;">RESERVATION</h3>
        </div>
        <div class="col-md-8" style="background-color: rgb(255,225,194);padding: 2% 0; border-top: solid 5px #4E0D4A;
        border-bottom: solid 5px #4E0D4A;">
                <div class="calendar-container" style="margin: 0 auto; display:block;">

                <div class="row">
                        <div class="col-md-4 text-center nights">
                                <span id ="nights" style="font-size:50px;font-style:italic;color:#660066">0</span><i class=" fa fa-moon"></i> <span id = "nightText" class="font-weight-bold"></span>
                                <span> <i class="fa fa-arrow-right ml-3"></i></span>
                        </div>
                        <div class="col-md-4 ">
                                <p>
                                        <input type="text" placeholder="CHECK IN" class="form-control textboxField mt-4 text-center" id = "arrival" readonly>
                                </p>
                        </div>
                        <div class="col-md-4 ">
                                <p>
                                        <input type="text" placeholder="CHECK OUT" class="form-control textboxField mt-4 text-center" id = "check_out" readonly>
                                </p>
                        </div>
                </div>

                <div class="row">
                        <div class="col mb-2">
                                <input type="text"  name ="dates" id="dates" class="form-control" style="display:none;" value = "{{ \Carbon\Carbon::now()->format('Y-m-d') }} - {{ \Carbon\Carbon::now()->addDay()->format('Y-m-d') }}">

                        </div>
                </div>
                <div class="row">
                        <button id = "btnSubmit" class="form-control btnColor m-auto">Submit</button>
                </div>
        </div>
</div>

<div class="container-fluid">
        <div class="row p-0 ">
                <div class="col-md-4">
                        <div class="row">
                                <div class="col">
                                        <div class="text-center">
                                                <p style="padding-top:50px;font-size:4.5em;font-family: 'Zhi Mang Xing', cursive;"> Camiguin Island</p>
                                                <p style="font-size:2em">  Major Tourist Spots</p>
                                        </div>

                                        <ul style="font-size:1.5em;">
                                                @forelse ($attraction as $item)
                                                <a href="{{  $item->link }} " target="_blank"><li><i class="fas fa-map-marker-alt  fa-1x  text-primary"></i> <span class="text-dark">{{$item->name}}</span> </li></a>
                                                @empty
                                                <span>Tourist Spots is on progress</span>
                                                @endforelse

                                        </ul>
                                </div>
                        </div>
                </div>

                <div class="col-md-8 p-0">
                        <img src="{{ asset ('img/UI/mapa.jpg') }}"  width="99%" alt="">
                </div>
        </div>
</div>



<div class="container-fluid">
        <div class="row">
                <div class="col-md-6" id="eventCorporate">
                        <h2 class="headingText" style="text-transform:uppercase;font-weight:bold;font-family: Arial, Helveticam sans-serif;" id="events">events</h2>
                        <div class="row">
                                <div class="col my-3 text-center">
                                        <img src="../img/UI/1.png" class="w-75" alt="">
                                </div>
                        </div>
                        <h4 style="color:rgb(102, 51, 153);font-weight:bold;text-transform:uppercase;font-family: Arial, Helvetica, sans-serif;"></h4>
                        <p>Conduct meetings, conferences and other corporate events in a setting that gives you more than just business power-ups</p>
                        <p>Here at <strong><i>Havendwell</i></strong>, you may opt to choose among 3 function rooms that can best suit yout purpose. Each of these venues can be set up in classroom style, U-shape and theater. If you opt for something beyond ordinary, you may also choose to have an outdoor set-up</p>
                        <div class="row">
                                <div class="col-md-6">
                                        <ul class="rooms">
                                                <li>&nbsp;Wi-Fi internet access</li>
                                                <li>&nbsp;LCD projector</li>
                                                <li>&nbsp;Projector screen</li>
                                                <li>&nbsp;Sound system</li>
                                        </ul>
                                </div>
                                <div class="col-md-6 mt-4">
                                        <button class="px-4 btn-Event" style=""  onClick="document.location.href='/functions-quotation'">Request Quotation</button>
                                </div>
                        </div>
                </div>

                <div class="col-md-6" style="background-color: rgb(255,225,194);" id="eventWedding">
                        <h2 class="headingText" style="padding-left:50px;text-transform:uppercase;font-weight:bold;font-family: Arial, Helveticam sans-serif;">&nbsp;</h2>
                        <div class="row">
                                        <div class="col my-3 text-center">
                                                <img src="../img/UI/2.png" class="w-75" alt="">
                                        </div>
                        </div>
                        <h4 style="color:rgb(102, 51, 153);font-weight:bold;text-transform:uppercase;font-family: Arial, Helvetica, sans-serif;">Weddings & Events</h4>
                        <p>Life is full of wonderful surprises and moments best remembered for all time. Whether you are exchanging vows, celebrating life or simply rejoicing milestones, spending a special moment at Havendwell will be worthwhile</p>
                        <p>Your photos for these precious times will look even more stunning with the pecturesque scenery gracing the background, as well as the artistic cultures dotting every corner of the resort</p>
                        <div class="container">
                                <div class="row">
                                        <div class="col mt-2">
                                                <button class="px-4 btn-Event" style="" onClick="document.location.href='/functions-quotation'">Request Quotation</button>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
        </div>
</div>

<div class="container-fluid">
        <div class="row">
                <div class="col" style="background-color:#ffe1c2;">
                        <h2 class="headingText" style="padding: 20px 0 0 50px;text-transform:uppercase;font-weight:bold;font-family: Arial, Helveticam sans-serif;" id="about-us">about us</h2>
                </div>
                <div class="col mobileDesign2"></div>
        </div>
        <div class="row">
                <div class="col-md-6 aboutCont" style="background-color:#ffe1c2;">
                        <h3 style="color:rgb(102, 51, 153);font-weight:bold;text-transform:uppercase;font-family: Arial, Helvetica, sans-serif;">History</h3>
                        <p><strong>Havendwell Resort</strong> is a family owned tourism establishment in the Province of Camiguin with business addresses at Rizal St., Upper Poblacion, Mambajao, Camiguin and Looc, Yumbing, Mambajao, Camiguin.</p>
                        <p>Havendwell is one of the newest resorts in the island offering quality standard of facilities and services. It is designed with a sense of a peaceful community and a relaxed intimate atmosphere, making it perfect for families and couples for whom a tranquil envrironment is an important part of the holiday experience.</p>

                        <div id="showReadmore1">
                                <p>Our Beach Resort is located at Looc, Yumbing, Mambajao, Camiguin is a total package of an amazing tranquil beachfront ambiance. The ocean view and spectacular sunset manifested the true essence of natures beauty. It has a very tranquil and relaxing atmosphere with the added value of its proximity to the very beautiful White Island which is one of the most popular tourist destinations in the island. <br>
                                </p>
                                <p>Our Garden resort  surrounded with variety of foliage and blooms is located at Umycco Upper Poblacion, Mambajao Camiguin.</p>
                                <p>A location with high proximity to the central business district of Mambajao which is Barangay Poblacion. The resort has a great advantage in terms of its access with only a walking distance to the trading center like public market, malls, groceries, drugstores and financial institutions like bank and money changers</p>
                                <p>It is also near to the government facilities like hospitals and other health facilities. Its also 2-3 minutes drive away from the Airport.</p>
                        </div>
                        <button class="readMore px-3 mb-3" id="readMore" style="background-color:rgb(225, 153, 51);color:white;border:none;float:right;">More Details</button>
                </div>

                <div class="col-md-6 my-auto aboutCont">
                        <h3 style="color:rgb(102, 51, 153);font-weight:bold;text-transform:uppercase;font-family: Arial, Helvetica, sans-serif;">MISSION</h3>
                        <p>We are dedicated to provide the highest standards quality service in order to serve our customers with delightful experiences that will surely exceed their expectations. As an agent of sustainable ecotourism we are committed to help preserve Camiguin's natural beauty and promote the island's local tourism</p>
                        <h3 style="color:rgb(102, 51, 153);font-weight:bold;text-transform:uppercase;font-family: Arial, Helvetica, sans-serif;">VISION</h3>
                        <p>To be acknowledged as the best nature-friendly resort in Camiguin equipped with globally competitive facilities and amenities with an outstanding customer-oriented service and management</p>
                </div>
        </div>
</div>
