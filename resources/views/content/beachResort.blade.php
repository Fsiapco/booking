@extends('layouts.master')

@section('style')
  <style scoped>
    .card-1 {
  padding: 10px 10px 10px 10px;
  box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);
  }
  .table-responsive>.table>tbody>tr>td{
      horizontal-align: middle;
  }
  .card {
    margin: 0 auto; /* Added */
          float: none; /* Added */
          margin-bottom: 10px; /* Added */
  }
  .modal-body {
    position: relative;
    overflow-y: auto;
    max-height: 800px;
    max-width: 800px;
    padding: 15px;
}
  .hr-design{
    height: 10px;
    color: rebeccapurple;
    background-image: linear-gradient(-45deg,
      transparent,
      transparent 25%,
      currentColor 25%,
      currentColor 50%,
      transparent 50%,
      transparent 75%,
      currentColor 75%);
    background-size: 10px 10px;
    width: 1100px;
  }
  .amenities {
  list-style: none;
  }
  .checkmark:before {
  content: '✓  ';
  }
  </style>
@endsection
@section('content')
<div id="carouselIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselIndicators" data-slide-to="1"></li>
      <li data-target="#carouselIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block vw-100 vh-100" src="/img/beach-resort/image-carousel/beachfront-view.jpg" alt="beach front">
        <div class="carousel-caption ">
          <h1 class="text-bold fontCarousel"style="padding-bottom:150px;">Havendwell Beach Resort</h1>
        </div>
      </div>
      <div class="carousel-item">
        <img class="d-block vw-100 vh-100" src="/img/beach-resort/image-carousel/room-with-sunset1.jpg" alt="sunset room">
        <div class="carousel-caption ">
          <h1 class="text-bold fontCarousel"style="padding-bottom:150px;">Havendwell Beach Resort</h1>
        </div>
      </div>
      <div class="carousel-item">
        <img class="d-block vw-100 vh-100" src="/img/beach-resort/image-carousel/room-with-sunset3.jpg" alt="sunset room 2">
        <div class="carousel-caption ">
          <h1 class="text-bold fontCarousel"style="padding-bottom:150px;">Havendwell Beach Resort</h1>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div>


<br>
<div class="container"><hr class="hr-design">
  <div class="row m-5 justify-content-center">
    <div class="col-md-5 offset-md-1">
      <div class=" p-3">
        <h2 class="text-bold">Our Amenities</h2>
        <ul class="amenities">
          @foreach ($amenities as $amenities)
           <li style="font-size:15px" class="checkmark text-bold"> {{ $amenities->amenities }}</li>
          @endforeach
        </ul>
      </div>
    </div>
    <div class="col-md-5">
      <div class=" p-3">
        <h2 class="text-bold">Free Breakfast</h2>
        <div>
          <p>Php <span class="text-bold">700 </span>/ pax <span class="text-bold">for extra bed</span></p>
        </div>
      </div>
    </div>
  </div><hr class="hr-design">
</div>


<div class="container">
  <div class="row">
    @forelse ($room as $data)
    <div class="col-md-3">
      <div class="card pt-4" style="width:16rem;">
        <div class="card-body text-center">
          <h5 class="card-title text-bold text-uppercase">{{ $data->type }}</h5>
          <p class="card-text">&#8369;<span class="text-bold">{{$data->price}} Php</span>/&nbsp;night&nbsp;<span class="text-bold">for {{ $data->guests }}  pax</span></p>
        <div class="collapse text-justify" id="amenitiesCollapse-{{$data->id}}">
            <p>{{$data->description}}</p>
            <ul>
                @forelse ($data->room_amenities as $amenities) 
                  <li class="card-text">{{ $amenities->amenities }}</li>
                @empty
                <span> No Amenities Available for this Room</span>     
                @endforelse
            </ul>
            <div class="text-center mt-2">
            <button type="button" class="mx-auto btn btn-primary bookBtn" room-id="{{$data->id}}" >Book Now</button>
            </div>
          </div>
        </div>
        <div class="card-footer bg-white text-center">
          <button class="btn btn-Amenities" type="button" data-toggle="collapse" data-target="#amenitiesCollapse-{{$data->id}}" aria-expanded="false" aria-controls="amenitiesCollapse">More Details</button>
        </div>
      </div>
    </div>
    @empty
    <span>Sorry No Room Available</span>
    @endforelse
  </div>
</div>

@include('modal.gardenResortModal')

@endsection

@section('script')
<script src="{{ asset('js/roomValidation.js') }}"></script>

    <script >
        $(document).ready(function(){

          
          $('#gardenViewRoom').on('hide.bs.modal', function (e) {
            location.reload();
          })

          $('.bookBtn').click(function(){
            let headers = new Headers();
            headers.append('Authorization', this.authToken);
            headers.append('Content-Type', 'application/json');
            var id = $(this).attr('room-id');
            $('#roomDate').val(' ')
            $('.nights').val(' ')

            //fetch Package Details
          
            $.ajax({
                url: '/Room-Modal/'+ id,
                method: 'GET',
                dataType : 'json',
                headers: {  'Access-Control-Allow-Origin': '*' },
                data:'_token = <?php echo csrf_token() ?>',
                success : function(data){
                
              
                    $('.image').attr('src', '/img/rooms/'+ data['photo']);
                    $('#exampleModalLongTitle').html(data['type']);
                    $('#description').html(data['description']);
                    $('span#price').html(data['price']);
                    $('span#guest').html(data['guests']);
                    $('#priceperNight').val(data['price']);
                    $('#roomID').val(data['id']);
                    $('span#queenBed').html(data['queenbed']);
                    $('span#singleBed').html(data['singlebed']);
                    jQuery.each(data.room_details, function(index, item) {
                      $('span#room_no').html(item.room_no);
                      $('#room_details_id').val(item.id);
                    });
                    jQuery.each(data.room_amenities, function(index, item) {
                      $("span#amenities").append(item.amenities + ',');
                       
                    });
                 

                    $('#gardenViewRoom').modal('show');
                   
                }
            });

     
            $.getJSON('/getbookRoom/'+id)
              .done(function(data) {
                
                  var datesArrays = data.dates;
                  var input = document.getElementById('roomDate');
                  var dateArray = [].concat.apply([], datesArrays); 
                  var datepicker = new HotelDatepicker(input,{
                  format: 'YYYY-MM-DD',
                  minNights: 1,
                  showTopbar: false,
                  disabledDates:dateArray,
                  onSelectRange: function() {
                   
                      var values = datepicker.getValue();
                      var nights = datepicker.getNights();
                      var dates = values.split(/ - /);
                      var Arrival =  $("#packageArrival").val(dates[0]);
                      var Check_out = $("#packagecheck_out").val(dates[1]);
                      var price =  $('#priceperNight').val();
                      var total_price = nights * price;
                    
                      $('#cost').val(total_price);
                     var final =  $('#roomPrice').val(total_price);
                     var x = $('#roomPrice').val();
                     $('span#totalToPaid').text(x);
                      $(".nights").val(nights);

                     }
                     
                    
                    });
            
                   
              });

              $('.paymenttype').change(function(){
                      if($(this).val() == 'Partial Payment'){
                      var partial =  $('#cost').val() / 2 ;
                      $('#roomPrice').val(partial);
                      var x = $('#roomPrice').val();
                     $('span#totalToPaid').text(x);
                      }else{
                          var full =  $('#cost').val() * 2;
                          $('#roomPrice').val(full);
                          var x = $('#roomPrice').val();
                         $('span#totalToPaid').text(x);
                      }
              
              });

            

               $(".request").click(function(){
                        var request = [];
                        var requestSubtotal = 0;
                        var requestValue = [];
                        $.each($("input[name='request']:checked"), function(){
                            request.push({
                                'request' : $(this).val(),
                                'price' : $(this).data("price")
                            });

                        });
                    
                        for (var i = 0; i < request.length; i++) {
                            requestSubtotal += request[i].price << 0;
                            requestValue.push(request[i].request);
                        }
                        
                    $("#requestValue").val(requestValue);
                    $("#requestTotal").val(requestSubtotal);

                    var totalCost = $('#cost').val();
                   
                    var subTotal = $('.SubTotal').val();
                    var total =  parseInt(totalCost) + parseInt(subTotal) ;
                    var couter = $('#counter').val(total);
                    var val = $('#counter').val();
                   
                    var final = $('#roomPrice').val(val);
                    var x = $('#roomPrice').val();
                     $('span#totalToPaid').text(x);
                    });              
          
           

              
          });

        
         
          
          $('#terms').click(function(){
                if($(this).prop("checked") == true){
                    $('.BookNow').removeAttr('disabled');
                }
                else if($(this).prop("checked") == false){
                    $('.BookNow').attr('disabled','disabled');
                }
            });

              $('.BookNow').click(function(e){
                if($('#gardenRoom').valid()){
                    $('.BookNow').attr('type','submit');
                  
                }
            });
        });
    </script>
@endsection