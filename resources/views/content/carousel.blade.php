<div class="container-fluid">
  <div class="row text-center">
    <div class="col-md-6 p-0">
      <img src="{{asset('img/UI/Untitled_Panorama2.jpg')}}" class="w-100" alt="">
      <a href="/garden-resort" class="px-3 mx-auto rounded-pill btn btn-white">Garden Resort</a>
 
    </div>
    <div class="col-md-6 p-0">
      <img src="{{asset('img/UI/beachfront-view.jpg')}}" class="w-100" alt="">
      <a href="/beach-resort" class="px-3 mx-auto rounded-pill btn btn-white">Beach Resort</a>
   
    </div>
  </div>
</div>

