@extends('layouts.master')
@section('style')
        <style>
                .error{
                        color:red !important;
                },
                .lds-ring {
  display: inline-block;
  position: relative;
  width: 80px;
  height: 80px;
}
.lds-ring div {
  box-sizing: border-box;
  display: block;
  position: absolute;
  width: 64px;
  height: 64px;
  margin: 8px;
  border: 8px solid #fff;
  border-radius: 50%;
  animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
  border-color: #fff transparent transparent transparent;
}
.lds-ring div:nth-child(1) {
  animation-delay: -0.45s;
}
.lds-ring div:nth-child(2) {
  animation-delay: -0.3s;
}
.lds-ring div:nth-child(3) {
  animation-delay: -0.15s;
}
@keyframes lds-ring {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}

        </style>
@endsection
@section('content')

<div class="parallax p-5" style=" background-image: url('img/UI/img5.jpg');" id="contact-us"  >
        <div class="row  justify-content-center" data-aos="zoom-in"
        data-aos-duration="1000">
                <div class="col-md-5 p-0  mt-4" style="border-bottom-right-radius:10px;border-top-left-radius:10px;background-color:rgba(225, 225, 225, 0.9);box-shadow:0px 0px 5px black">
                        <div class="p-2 text-white" style="border-top-left-radius:10px;background-color:rgb(99, 81, 206)">
                                <h3 class="font-weight-bold text-center mt-2" id="contact-title">
                                        Contact
                                </h3>

                        </div>


                        <div class="row  justify-content-center" >
                                <div class="col-md-9 p-4">
                                        <form  id="QuotationForm" >
                                        <div>
                                                <span class="font-weight-bold">
                                                        Name:
                                                </span>
                                        </div>
                                        <div>
                                                <input class="form-control mb-2 @error('name') is-invalid @enderror"  value="{{ old('name') }}"  type="text" minlength="2" id="name" name="name" required/>
                                                @if($errors->any())
                                                <h6 class="error text-bold">{{$errors->first('name')}}</h4>
                                                @endif
                                        </div>
                                        <div>
                                                <span class="font-weight-bold">
                                                        Email:
                                                </span>
                                        </div>
                                        <div>
                                                <input  class="form-control mb-2 @error('email') is-invalid @enderror"  value="{{ old('email') }}"  id="email" name="email" type="email" required/>
                                                @if($errors->any())
                                                <h6 class="error text-bold">{{$errors->first('email')}}</h4>
                                                @endif
                                        </div>
                                        <div>
                                                <span class="font-weight-bold">
                                                        Contact Number:
                                                </span>
                                        </div>
                                        <div>
                                                <input  class="form-control mb-2 @error('number') is-invalid @enderror"  value="{{ old('number') }}" type="number"  id="number" name="number"  onKeyPress="if(this.value.length==13) return false;" onkeydown="javascript: return event.keyCode == 69 ? false : true" required/>
                                                @if($errors->any())
                                                <h6 class="error text-bold">{{$errors->first('number')}}</h4>
                                                @endif
                                        </div>
                                        <div>
                                                <span class="font-weight-bold">
                                                        Type of Event:
                                                </span>
                                        </div>
                                        <div>
                                                <input type="text" class="form-control mb-2" id="type" name="type" required/>
                                                @if($errors->any())
                                                <h6 class="error text-bold">{{$errors->first('type')}}</h4>
                                                @endif
                                        </div>
                                        <div class="row">

                                            <div class="col-6">
                                                    <span class="font-weight-bold">
                                                        Date  of the event:
                                                    </span>
                                                    <input type="date" class="form-control mb-2" id="date" name="date" required/>
                                                    @if($errors->any())
                                                    <h6 class="error text-bold">{{$errors->first('date')}}</h4>
                                                    @endif
                                            </div>
                                            <div class="col-6">
                                                    <span class="font-weight-bold">
                                                        Time of the event:
                                                    </span>
                                                    <input type="time" class="form-control mb-2" id="time" name="time" requied/>
                                                    @if($errors->any())
                                                    <h6 class="error text-bold">{{$errors->first('time')}}</h4>
                                                    @endif
                                            </div>
                                        </div>
                                        <div>
                                                <span class="font-weight-bold">
                                                        No. of Pax:
                                                </span>
                                        </div>
                                        <div>
                                                <input type="number" class="form-control mb-2" id="pax" name="pax"  required/>
                                                @if($errors->any())
                                                <h6 class="error text-bold">{{$errors->first('pax')}}</h4>
                                                @endif
                                        </div>
                                        <div>
                                                <span class="font-weight-bold">
                                                        Additional Remarks:
                                                </span>
                                        </div>

                                        <div class="mt-3">
                                                <textarea  placeholder="Message here..." id="remarks" style="font-style:italic;"  class="form-control @error('message') is-invalid @enderror mb-4"  value="{{ old('message') }}  " cols="30" rows="10"  id="message" name="message"  required></textarea>

                                        </div>
                                        <button class="pl-4 pr-4" id="quoationFormSubmit"  style="margin:0 auto;display:block;background-color: rgb(225, 153, 51);color:white;border:none; text-transform:uppercase;">
                                                Submit
                                                <span id="loading" style="display:none;"><div class="lds-ring"><div></div><div></div><div></div><div></span>
                                        </button>
                                                
                                        </form>
                                </div>
                        </div>
                </div>
        </div>
</div>




@endsection
@section('script')
        <script>
              $('#quoationFormSubmit').click(function(e){
                e.preventDefault();

                // Show loading icon when click
                $("#loading").css("display", "block");
                var name = $('#name').val();
                var email = $('#email').val();
                var number = $('#number').val();
                var type = $('#type').val();
                var date = $('#date').val();
                var pax = $('#pax').val();
                var time = $('#time').val();
                var remarks = $('#remarks').val();

                if($('#QuotationForm').valid()){

                        $.ajax({
                                type        : 'POST',
                                url         :  'quotation-Page',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                data  :{
                                       name :name,
                                       email:email,
                                       number:number,
                                       type:type,
                                       pax:pax,
                                       date:date,
                                       time:time,
                                       remarks,remarks

                                },
                                // dataType    : 'json',
                                success: function(data){
                                        $("#loading").css("display", "none");
                                        document.getElementById('QuotationForm').reset();
                                        Swal.fire({
                                                title: 'Sucess!',
                                                text: 'Message Sent Successfully',
                                                type: 'success',
                                                showConfirmButton: true
                                        })
                                        // hide loading icon


                                }
                        });

                // $('#contactUsForm').submit();

                }else{
                        $("#loading").css("display", "none");
                        $('#QuotationForm').validate({
                                rules:{
                                        name: {
                                                required:true,
                                                minlength:2,
                                        },
                                        email:{
                                                required:true,
                                                email:true,
                                        },
                                        number:{
                                                required:true,
                                                number:true,
                                                minlength:5,
                                                maxlength:11,
                                        },
                                        pax:{
                                                required:true,
                                        },
                                        type:{
                                            required:true,
                                        },
                                        date:{
                                            required:true,
                                        },
                                        time:{
                                            required:true,
                                        }

                                }
                        });
                }
            });
        </script>

@endsection