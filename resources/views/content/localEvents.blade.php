@extends('layouts.master')
@section('content')
<div class="container-fluid">
        <div class="row">
            <img src="../img/local-events/event-banner.jpg" class="w-100" alt="">
        </div>
</div>
<table>
    <tr class="row p-4 my-4 bg-gray"  data-aos="fade-up" data-aos-duration="4000" style="display:flex; width: 80%; margin-left:10%; margin-right:10%;">
        <td class="col-md-4" style="height:auto">          
            <div class="text-center p-4">
                <h1 style="font-weight:bold;color:white" id="booking">Lanzones Festival</h1>
                <div class="card text-white">
                    <img class="card-img" style="height:200px;" src="/img/local-events/lanzones-festival.jpg" alt="Card image">
                </div>
            </div>
        </td>
        <td class="col-md-8 bg-dark locale-attraction-text p-5" id="text-attraction">
            <p>
                A two day grand festival of agri-cottages industry products in exhibits, barangay beautification, indigenous sports, tableau of local culture, grand parade of golden fruit found prolific and extra sweet in the entire province.
            </p>
            <p> The festival is Camiguin’s contribution to Mindanao as cultural destination.</p>
            <p>( 3rd weekend of October; movable )
            </p>
            <p >Photo Credits :<a href="https://www.rappler.com/life-and-style/travel/243462-photos-lanzones-festival-2019" target="_blank"> https://www.rappler.com/life-and-style/travel/243462-photos-lanzones-festival-2019</a></p>
        </td>
    </tr>
    <tr class="row p-4 my-4 bg-gray"  data-aos="fade-up" data-aos-duration="4000" style="display:flex; width: 80%; margin-left:10%; margin-right:10%;">
        <td class="col-md-4" style="height:auto">          
            <div class="text-center p-4">
                <h1 style="font-weight:bold;color:white" id="booking">San Juan sa Hibok-Hibok Festival</h1>
                <div class="card text-white">
                <img class="card-img" style="height:200px;" src="/img/local-events/san-juan-hibok-hibok-festival.jpg" alt="Card image">
            </div>
        </td>
        <td class="col-md-8 bg-dark locale-attraction-text p-5" id="text-attraction">
            <p>
                San Juan sa Hibok-Hibok Festival was held to honor St. John the Baptist. </p><p>Venues of the festival are usually Cabu-an and/or Agohay Beaches. Water sports like boat races, fluvial processions/parade and coronation of Miss Hibok Hibokan are conducted.</p><p> Celebrated in the entire province, residents go to the nearest beach and while away time until late afternoon.
            </p>
            <p>
                ( June 24 )
            </p>
            <p >Photo Credits :<a href="https://www.hellotravel.com/events/san-juan-sa-hibok-hibok-festival" target="_blank"> https://www.hellotravel.com/events/san-juan-sa-hibok-hibok-festival</a></p>
            
        </td>
    </tr>
    <tr class="row p-4 my-4 bg-gray"  data-aos="fade-up" data-aos-duration="4000" style="display:flex; width: 80%; margin-left:10%; margin-right:10%;">
        <td class="col-md-4" style="height:auto">          
            <div class="text-center p-4">
                <h1 style="font-weight:bold;color:white" id="booking">Panaad</h1>
                <div class="card text-white">
                <img class="card-img" style="height:200px;" src="/img/local-events/panaad.jpg" alt="Card image">
            </div>
        </td>
        <td class="col-md-8 bg-dark locale-attraction-text p-5" id="text-attraction">
            <p>
                A pilgrim’s yearly trek around the island in observance of the Lenten season. 
            </p>
            <p>Thousands of visitors make this island a Mecca as they converge at Bonbon for rituals or just getting together.
            </p>
            <p>
                ( Holy Week )
            </p>
            <p >Photo Credits :<a href="http://www.phtourguide.com/camiguin-walkway-to-mount-vulcan-and-stations-of-the-cross/" target="_blank"> http://www.phtourguide.com/camiguin-walkway-to-mount-vulcan-and-stations-of-the-cross/</a></p>
        </td>
    </tr>
    <tr class="row p-4 my-4 bg-gray"  data-aos="fade-up" data-aos-duration="4000" style="display:flex; width: 80%; margin-left:10%; margin-right:10%;">
        <td class="col-md-4" style="height:auto">          
            <div class="text-center p-4">
                <h1 style="font-weight:bold;color:white" id="booking">May Festival</h1>
                <div class="card text-white">
                <img class="card-img" style="height:250px;" src="/img/local-events/may-festival.jpg" alt="Card image">
            </div>
        </td>
        <td class="col-md-8 bg-dark locale-attraction-text p-5" id="text-attraction">
            <p>
                The festival, held in Camiguin Island during May, celebrates the Local town’s holiday where it can either be a celebration of a saint or the lord.
            
                It was a very effective tool the Spaniards used to help Filipinos understand the religion and uplift their faith to Roman Catholic.
            </p>
            <p>
                It has been marked deep within Filipino culture where even after the liberation from Spain, people still practice these local festivities.
            </p>
            <p >Photo Credits :<a href="http://www.traveltothephilippines.info/2017/04/16/join-the-vibrant-and-exciting-festivals-in-camiguin/camiguin-may-festival/" target="_blank"> http://www.traveltothephilippines.info/2017/04/16/join-the-vibrant-and-exciting-festivals-in-camiguin/camiguin-may-festival/</a></p>
        </td>
    </tr>
</table>
@endsection