@extends('layouts.master')
@section('style')
        <style scoped>
                tr:nth-child(even) td {
                        background: #FFFFFF;
                }
                tr:nth-child(odd) td {
                        background: #343A40;        
                }
                tr:nth-child(odd) td #booking {
                        color:white !important;
                }
                tr:nth-child(even) td #booking {
                        color:#343A40 !important;
                }
                tr:nth-child(even) #text-attraction{
                        color: white !important;
                }
                tr:nth-child(odd) #text-attraction{
                        color: black !important;
                        background: #FFFFFF !important;
                }
                
        </style>
@endsection

@section('content')

<div class="container-fluid">
        <div class="row">
                <img src="../img/local-delicacy/local-delicacies-header.jpg" class="w-100" alt="">
        </div>
</div>

<table >
    <tr class="row p-4 my-4 bg-gray"  data-aos="fade-up" data-aos-duration="4000" style="display:flex; width: 80%; margin-left:10%; margin-right:10%;">
        <td class="col-md-4" style="height:auto">          
            <div class="text-center p-4">
                <h1 style="font-weight:bold;color:gray"   id="booking">SUROL</h1>
                <div class="card text-white">
                <img class="card-img" style="height:200px;"src="{{ asset('../img/local-delicacy/delicacy1.jpg') }}" alt="Card image">
            </div>
        </td>
        <td class="col-md-8 bg-dark locale-attraction-text py-5" id="text-attraction">
            <p>Surol is a chicken soup dish made of free range (native) chicken cooked in coconut milk.</p>
            <p>It's basically like nothing like <i>tinolang manok</i> only with <i>gata</i> (coconut milk). </p><p>It's pretty much available everwhere - in resturants and <i>carinderias</i> (roadside eateries).</p><br>
            <p >Photo Credits:<a href="https://escapemanila.com/2017/11/camiguin-food-guide-must-try-food.html" target="_blank"> https://escapemanila.com/2017/11/camiguin-food-guide-must-try-food.html</a></p>
        </td>
    </tr>
    
    <tr class="row p-4 my-4 bg-gray"  data-aos="fade-up" data-aos-duration="4000" style="display:flex; width: 80%; margin-left:10%; margin-right:10%;">
        <td class="col-md-4" style="height:auto">          
            <div class="text-center p-4">
                <h1 style="font-weight:bold;color:gray"   id="booking">LANZONES ICE CREAM</h1>
                <div class="card text-white">
                <img class="card-img" style="height:200px;"src="{{ asset('../img/local-delicacy/delicacy3.jpg') }}" alt="Card image">
            </div>
        </td>
        <td class="col-md-8 bg-dark locale-attraction-text py-5" id="text-attraction">
            <p>Camiguin is popular for its sweet lanzones, thus the island celebreates Lanzones Fesitaval every third week of October. </p>
            <p>Lanzones thress only bear fruits once a year so you may not find the fruit on some months. </p>
            <p>If you can't find the fresh fruits, then try their local version of Lanzones Ice Cream.</p><br>
            <p >Photo Credits:<a href="https://escapemanila.com/2017/11/camiguin-food-guide-must-try-food.html" target="_blank"> https://escapemanila.com/2017/11/camiguin-food-guide-must-try-food.html</a></p>
        </td>
    </tr>

    <tr class="row p-4 my-4 bg-gray"  data-aos="fade-up" data-aos-duration="4000" style="display:flex; width: 80%; margin-left:10%; margin-right:10%;">
        <td class="col-md-4" style="height:auto">          
            <div class="text-center p-4">
                <h1 style="font-weight:bold;color:gray"   id="booking">PASTEL</h1>
                <div class="card text-white">
                <img class="card-img" style="height:200px;"src="{{ asset('../img/local-delicacy/pastel.png') }}" alt="Card image">
            </div>
        </td>
        <td class="col-md-8 bg-dark locale-attraction-text py-5" id="text-attraction">
            <p>Pastel is a proudly Camiguin delicacy. It's a sweet bun usually filled with <i>yema</i>. 
            </p>It also comes with other fillings like ube, macapuno, among others.</p><p> In the 90s, it used to be available in Camiguin only.</p>
            <p> Nowadays, it is widely available in other parts of the country.</p><br>

            <p >Photo Credits:<a href="https://vjandep.com/pastel/" target="_blank"> https://vjandep.com/pastel/</a></p>
        </td>
    </tr>
</table>

@endsection