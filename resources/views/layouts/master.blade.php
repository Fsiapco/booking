<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>HavendWell</title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css"> --}}
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/kronos.css') }}" rel="stylesheet">
    <link href="{{ asset('css/hotel-datepicker.css') }}" rel="stylesheet">

    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css'/>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css'/>
    <link href="https://fonts.googleapis.com/css?family=Cedarville+Cursive&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@8.11.7/dist/sweetalert2.min.css">
    <link href="https://fonts.googleapis.com/css?family=Questrial&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Yellowtail&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Luckiest+Guy&display=swap" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('../img/UI/sampleLogo.png') }}">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Zhi+Mang+Xing&display=swap" rel="stylesheet">

    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>



    @yield('style')
</head>

<body >
<div id="app">
    <div style="overflow-x:hidden">
        <div id="headNav">
            @include('partial.navigation')

        </div>
        <div class="modal hide fade" id="myModal">

        </div>
        @if (session('message'))
            <input type="checkbox"  name="triggerSwal" id="triggerSwal" checked style="display:none"/>
        @endif
        @include('modal.landingModal')
            @yield('content')

            <a href="#" id="back-to-top" title="Back to top"><i class="fas fa-angle-double-up fa-2x" aria-hidden="true"></i></a>
        @include('partial.footer')
    </div>

</div>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{asset('js/backtotop.js')}}"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/fecha/2.3.3/fecha.min.js'></script>
    <script src="{{ asset('js/hotel-datepicker.min.js') }}"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js'></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.11.7/dist/sweetalert2.all.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js'></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    {{-- <script src="{{ asset('js/kronos.js') }}"></script>   --}}



    <!-- Optional: include a polyfill for ES6 Promises for IE11 -->
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script>

    <script>
        $('.carousel').carousel({
            interval: 4000
        });

        $('.autoplay').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1000,
            responsive: [
                {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: false
                }
                },
                {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
                },
                {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
                }
            ]
        });
        $('.tour-autoplay').slick({
            dots: false,
            arrows: false,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 2500,
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [
                {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: false
                }
                },
                {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
                },
                {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
                }
            ]
        });
        $('.catering-event').slick({
            dots: false,
            arrows: false,
            infinite: true,
            autoplay: false,
            slidesToShow: 4,
            responsive: [
                {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                }
                },
                {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
                },
                {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
                }
            ]
        });

        new WOW().init();
        $(document).ready(function(){
            var pathname = window.location.pathname;
            console.log(pathname);
            if(pathname != '/'){
                $('#navBooking').hide();
                $('#navContact').hide();
                $('#navBookings').show();
                $('#navContacts').show();
            }
            if(pathname == '/van-transport'){
                $('#navBooking').hide();
                $('#navContact').hide();
                $('#navBookings').show();
                $('#navContacts').show();

            }
            if(pathname == '/'){
                $('#navBookings').hide();
                $('#navContacts').hide();

                $('#contact-title').html('Contact Us');
            }
            if(pathname == '/functions-quotation'){
                $('#contact-title').html('Request for Quotation')
            }
            if(pathname == '/garden-resort'){
                $('.logo').prop('src','/img/UI/gardenLogo.png')
            }
            if(pathname == '/beach-resort'){
                $('.logo').prop('src','/img/UI/beachLogo.png')
            }
            if(pathname == '/locale-Attraction'){
                $('body').css('background-image', "url('/img/UI/camiguin-white-sand.jpg')");
                $('body').css('background-blend-mode','overlay');
                $('body').css('background-color','rgba(255,255,255,0.5)');
                $('body').css('background-position','center');
                $('body').css('background-size', 'cover');
                $('body').css('background-repeat','no-repeat');
                $('body').css('background-attachment','fixed');
            }
            // alert(pathname);
            $(function() {
                $('.lazy').lazy();
            });
        });

        $(document).ready(function(){
            $("#readMore").click(function(){
                $("#showReadmore1, #showReadmore2").slideToggle("slow");
                if($(this).html() == 'Read More'){
                    $(this).html('Read Less');
                }
                else{
                    $(this).html('Read More');
                }
            });

            $('.readMore').click(function(){
                var $this = $(this);
                $this.toggleClass('readMore');
                if($this.hasClass('readMore')){
                    $this.text('More Details');         
                } else {
                    $this.text('Less Details');
                }
            });



            if($('input[name="triggerSwal"]').is(':checked'))
            {
                Swal.fire(
                'Sucessfully Booked!',
                'Thankyou For booking in our Website!',
                'success'
                )
            }

           
        });

    </script>
    @yield('script')

</body>
</html>
