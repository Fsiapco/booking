<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationPage extends Model
{
    protected $guarded = [];
    protected $table = 'quotations';
}
