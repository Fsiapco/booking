<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Package;
use App\PackageDetail;
use App\item;
class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  
      
        $package = Package::where('package_id',$request->package_id)->first();
      
        if($package === null){
            if(!isset($request->img)){
                $name = "packageImg.png";
                }
            $name = time().'.' . explode('/', explode(':', substr($request->img, 0, strpos($request->img, ';')))[1])[1];
            \Image::make($request->img)->fit(380,380)->save(public_path('img/package/').$name);
            $request['package_id'] = $request->package_id;
            $request['img'] = $name;
            $request['packagename'] = $request->namepackage;
            $request['details'] = $request->details;
            $request['total_price'] = $request->totalPrice;


            $package = Package::Create($request->only(['package_id','img','packagename','details','total_price']));

            $request['price'] = $request->totalPrice;

          
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return Item::with('package')->where('id',$request->id)->first();
        // return Item::find($request->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            
        return Item::with('package_details')->where('id',$id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $package = Package::where('package_id',$id)->first();
        $itemUpdate = Item::find($id);

        if($package->img == $request->img){
            $name = $package->img;
        }else{
            $packageImg = public_path('img/package/').$package->img;
            @unlink($packageImg);
            
            $name = time().'.' . explode('/', explode(':', substr($request->img, 0, strpos($request->img, ';')))[1])[1];
            \Image::make($request->img)->fit(380,380)->save(public_path('img/package/').$name);
        
        }
      
        $request['img'] = $name;
        $request['total_price'] = $request->totalPrice;
        $request['price'] = $request->totalPrice;

        $package->update($request->only(['package_id','img','packagename','details','total_price']));
   
      
        $itemUpdate->update($request->only(['price']));

        return $package;
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       

    }

     public function addtoPackage(Request $request){
      
        $item = item::find($request->id);

        $request['package_id'] = $request->packageId;
        $request['itemname'] = $item->itemname;
        $request['price'] = $item->price;
       
        $data = PackageDetail::create($request->only(['package_id','itemname','price']));
        return $data;
    }

    public function destroyItem($id){
        $data = PackageDetail::find($id);
        $data->delete();

        return $data;
    }



}
