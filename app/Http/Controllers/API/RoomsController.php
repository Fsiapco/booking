<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Room;
use App\Amenities;
use App\ n;
use Auth;
use App\RoomDetails;
use App\ExtraRequest;
use App\RoomAmenities;
class RoomsController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->branch_id == 3){
            return  Room::with('room_amenities','branch','room_details')->latest()->paginate(3);
        
        }else{
            return Room::with('room_amenities','branch','room_details')->where('branch_id',Auth::user()->branch_id)->latest()->paginate(3);
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
     
        $this->validate($request, [
            'type' => 'required|string|max:255',
            'price' => 'required',
            'guests' => 'required',
            'branch_id'=>'required'
        ]);

        if(!isset($request->photo)){
            $name = "gallery.png";
        }
        if($request->photo){
            $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];

            \Image::make($request->photo)->save(public_path('img/rooms/').$name);
        }
        $request['photo'] = $name;
        $request['branch_id'] = $request->branch_id ;
        $request['room_no'] = $request->room_no ;
        $request['queenbed'] = $request->queenBed;
        $request['singlebed'] = $request->singleBed;
        $rooms = Room::create($request->only(['photo','type','price','description','guests','branch_id','queenbed','singlebed']));
        

        $request['room_id'] = $rooms->id;

        $rooms->room_details()->create($request->only(['room_id','room_no']));
        foreach($request->value as $value){
            $request['amenities'] = $value['name'];
            $request['amenities_id'] = $value['code'];
            $rooms->room_amenities()->create($request->only(['amenities','amenities_id']));
            // return $request->amenities;
        }

      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Room::with('room_amenities','room_details')->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        
      
        $this->validate($request, [
            'type' => 'required|string|max:255',
            'price' => 'required',
            'guests' => 'required',
            
        ]);


        $currentPhoto = Room::findOrFail($id)->photo;
        if($request->photo == null){
            $request['photo'] = $currentPhoto;
        }elseif($currentPhoto == $request->photo ){
            $request['photo'] = $currentPhoto;
        }else{
            $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];

            \Image::make($request->photo)->save(public_path('img/rooms/').$name);

            if($currentPhoto != "gallery.png"){
                $roomPhoto = public_path('img/rooms/').$currentPhoto;
                @unlink($roomPhoto);
             
            }
            $request['photo'] = $name;
        }
      
        $request['queenbed'] = $request->queenBed;
        $request['singlebed'] = $request->singleBed;
        $request['bankBed'] = $request->bankBed;
        $request['doubleBed'] = $request->doubleBed;
        $request['branch_id'] = $request->branch_id;
        $room = Room::with('room_amenities','room_details')->whereId($id)->first();
        $room->update($request->only(['branch_id','photo','type','price','description','guests','queenbed','singlebed','bankBed','doubleBed']));
        $room->room_details()->update($request->only(['room_no']));
       


        if(!empty($room->room_amenities)){
            $room->room_amenities()->delete();
        }
        foreach($request->value as $value){
            $request['amenities'] = $value['name'];
            $request['amenities_id'] = $value['code'];
            $room->room_amenities()->updateOrCreate($request->only(['amenities','amenities_id']));
            // return $request->amenities;
        }
    
     
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $room = Room::findOrFail($id);

        $roomPhoto = public_path('img/rooms/').$room->photo;
        @unlink($roomPhoto);

        $room->room_amenities()->delete();
        $room->room_details()->delete();
        $room->delete();


        
    }


    // amenities list

    public function amenities(){
        return Amenities::all();
    }

    public function roomName(){
        return Room::with('room_details')->get();
    }

    public function filterBranch($id){
        if($id == 3){
            return  Room::with('room_amenities','branch')->latest()->paginate(3);
        
        }else{
            return Room::with('room_amenities','branch')->where('branch_id',$id)->latest()->paginate(3);
        }
    }   


  
}
