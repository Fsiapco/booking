<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\attraction;
class AttractionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return attraction::latest()->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
            'img' => 'required',
            'name' => 'required',
            'details'=>'required',
        ]);

        $name = time().'.' . explode('/', explode(':', substr($request->img, 0, strpos($request->img, ';')))[1])[1];

        \Image::make($request->img)->fit(1000,667)->save(public_path('img/locale-attraction/').$name);

        $request['img'] = $name;
        $request['price'] = $request->price;
        $request['name'] = $request->name;
        $request['details'] = $request->details;
        $request['link'] = $request->link;
       
        $attraction =  attraction::Create($request->only(['img','name','price','details','link']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return attraction::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attrac = attraction::find($id);

        $this->validate($request,[
            'img' => 'required',
            'name' => 'required',
            'details'=>'required',
        ]);

        if($attrac->img == $request->img){
            $name = $attrac->img;
        }else{
            $attracPhoto = public_path('img/locale-attraction/').$attrac->img;

            if(File_exists($attracPhoto)){
                @unlink($attracPhoto);
                $name = time().'.' . explode('/', explode(':', substr($request->img, 0, strpos($request->img, ';')))[1])[1];
                \Image::make($request->img)->fit(1000,667)->save(public_path('img/locale-attraction/').$name);
            }    
      
        }
        

        $request['img'] = $name;
        $request['price'] = $request->price;
        $request['name'] = $request->name;
        $request['details'] = $request->details;
        $request['link'] = $request->link;
       
       $attrac->Update($request->only(['img','name','price','details','link']));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attrac = attraction::find($id);
        

        $attractionImg = public_path('img/locale-attraction/').$attrac->img;
        @unlink($attractionImg);
        $attrac->delete();
    }
}
