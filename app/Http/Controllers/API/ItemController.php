<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Item;
use App\PackageDetail;
use App\Package;
class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = Item::with('branch')->where('type','item')->latest()->paginate(5);
        $package = Item::with('package')->where('type','package')->latest()->paginate(3);

        return response()->json([
            'item' =>  $item ,
            'package' => $package
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     

        $this->validate($request,[
            'itemName' => 'required',
            'price' => 'required|integer|not_in:0',
        ]);
        
        $item =  Item::create($request->all());

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Item::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Item::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'itemName' => 'required',
            'price' => 'required|integer|not_in:0',
            'pax' => 'required|integer|not_in:0',
            'branch_id' => 'required'

       
        ]);
        
        $item = Item::find($id);

        $item->itemname = $request->itemName;
        $item->price = $request->price;
        $item->type = $request->type;
        $item->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $photo =  Item::with('package_details','package')->find($id);
      
       
      
        if($photo->type == 'package'){
            if($photo->package->isEmpty()){
                $item =  Item::with('package_details')->find($id);
                $item->package_details()->delete();
                $item->delete();
            }else{
                foreach ($photo->package as $package) {
                    $img = $package->img;
                }
                    $packageImg = public_path('img/package/').$img;
            
                  
                        @unlink($packageImg);
                        $item =  Item::with('package_details')->find($id);
                        $item->package_details()->delete();
                        $item->package()->delete();
                        $item->delete();
                    
            }
           
               
          
       
        }else{
            $item =  Item::with('package_details')->find($id);
            $item->delete();
        }
     
       
    }

    public function loadpackageitem($id){
      
        return Item::with('package_details')->where('id',$id)->first(); 
    }
    // public function createPackage(Request $request){
        
    //     return item::with('package','package_details')->where('id',$request->id)->first(); 
       
    // }
    // public function updateprice(Request $request){
    //     $item = Item::find($request->id);
    //     $item->price = $request->totalPrice;
    //     $item->update();
        
    //     return $item ;
    // }
   
    // public function deletetoPackage($id){
    //     $pack = PackageDetail::find($id);
    //     $pack->delete();
     
      
    // }
   

}
