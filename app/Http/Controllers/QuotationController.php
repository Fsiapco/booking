<?php

namespace App\Http\Controllers;

use App\Mail\Quotation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use  App\QuotationPage;
class QuotationController extends Controller
{
    public function send(Request $request){


        Mail::to('havenwellresort@gmail.com')->send(new Quotation($request->all()));

        QuotationPage::create($request->all());

        return redirect('functions-quotation');
    }
}
