<?php

namespace App\Http\Controllers;

use App\Reservation;
use App\Customer;
use Illuminate\Http\Request;
use App\Room;
use App\RoomDetails;
use DateTime;
use DatePeriod;
use DateInterval;
use Session;
use App\ExtraRequest;
use Srmklive\PayPal\Services\ExpressCheckout;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        return Reservation::with('customer','paypal_payments','rooms')->Where('status','Check in')->orWhere('status','Reserved')->paginate(2);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

;            $dates = explode(' - ', $request->dates);
            $checkIn = $dates[0];
            $checkOut = $dates[1];


              // Paypal Start
          $provider = new ExpressCheckout;


            $options = [
                'BRANDNAME' => 'Havendwell',
               // 'LOGOIMG' => 'https://img1-placeit-net.s3-accelerate.amazonaws.com/uploads/stage/stage_image/31594/large_thumb_1599-logo-cheerleader-alt-5__1_.jpg',
                'CHANNELTYPE' => 'Merchant'
            ];

            $provider->addOptions($options);
            $data = [];

            $invoiceId = uniqid();
            if($request->packageId != ''){
                $roomDetails = RoomDetails::with('reservations')->find($request->room_detail_id);
            }else if($request->room_id != '' ){
                $roomDetails = RoomDetails::with('reservations')->find($request->room_detail_id);
            }else{

                $roomDetails = RoomDetails::with('reservations')->find($request->get('room_detail_id'));
            }

            $room_name = Room::find($roomDetails->room_id)->type;
            $price = $request->total_cost;
            $qty = 1;


            $data = $this->cartData($invoiceId, $room_name, $price, $qty);

            $response = $provider->setExpressCheckout($data);

            if($request->packageId != ''){
                 Session::put([
                    'room_id' => $request->SelectedRoom,
                    'checkIn' => $checkIn,
                    'checkOut' => $checkOut,
                    'room_detail_id' => $request->room_detail_id,
                    'room_name' => $room_name,
                    'total_cost' => $request->total_cost,
                    'firstname' => $request->firstname,
                    'lastname' => $request->lastname,
                    'email' => $request->email,
                    'nationality' => $request->nationality,
                    'contact' => $request->contact_number,
                    'request' => 'None',
                    'night'=> $request->night,
                    'price' => $request->priceNight,
                    'charges' => $request->charges,
                    'paymentMethod' => $request->paymentMethod,
                    'packageID' => $request->packageId
                ]);
            }else if($request->room_id != ''){
                Session::put([
                    'room_id' => $request->room_id,
                    'checkIn' => $checkIn,
                    'checkOut' => $checkOut,
                    'room_detail_id' => $request->room_detail_id,
                    'room_name' => $room_name,
                    'total_cost' => $request->total_cost,
                    'firstname' => $request->firstname,
                    'lastname' => $request->lastname,
                    'email' => $request->email,
                    'nationality' => $request->nationality,
                    'contact' => $request->contact_number,
                    'request' => $request->specialrequest,
                    'night'=> $request->night,
                    'charges' => $request->charges,
                    'paymentMethod' => $request->paymentMethod,

                ]);
            }else{
                Session::put([
                    'checkIn' => $checkIn,
                    'checkOut' => $checkOut,
                    'room_detail_id' => $request->get('room_detail_id'),
                    'room_name' => $room_name,
                    'total_cost' => $request->get('total_cost'),
                    'firstname' => $request->get('firstname'),
                    'lastname' => $request->get('lastname'),
                    'email' => $request->get('email'),
                    'nationality' => $request->nationality,
                    'contact' => $request->get('contact_number'),
                    'request' => $request->get('special_request'),
                    'night'=> $request->get('night'),
                    'price' => $request->priceNight,
                    'charges' => $request->charges,
                    'paymentMethod' => $request->paymentMethod
                ]);

            }



            return redirect($response['paypal_link']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function show($id, $checkIn, $checkOut)
    {
        $guestRequest =  ExtraRequest::all();

        $rooms  = Room::with(['room_images','room_amenities',
            'room_details' => function($child) use ($id, $checkIn, $checkOut){
                return $child->where('room_id', $id)->with('reservations')->whereNotIn('room_no', function($query) use ($checkIn, $checkOut){
                    return $query->select('room_no')
                     ->from('reservations')
                     ->where([
                         ['reservations.check_out', '>=', $checkIn],
                         ['reservations.check_in', '<=', $checkOut]
                     ])->orWhere([
                         ['reservations.check_out', '<=', $checkIn],
                         ['reservations.check_in', '>=', $checkOut],
                     ]);
                });
            }
        ])->where('id', $id)->first();

        return view('room.book', compact('rooms', 'checkIn', 'checkOut','guestRequest'));
    }

    public function getDates($id){

        $room = RoomDetails::with('reservations')->find($id);

        $dates = [];
        foreach($room->reservations as $key => $reservation){
            $dates[] = $this->getDatesFromRange($reservation->check_in, $reservation->check_out);
        }

        return response()->json(['dates' => $dates]);
    }

    public function PackageDates($id){
        $reserved =  Room::with('room_details','reservations')->where('branch_id',$id)->get();

        return response()->json(['room' => $room]);
    }

    public function AvailableRoom($id){

       $room =  Room::with('room_details','reservations')->where('branch_id',$id)->get();

       return response()->json(['room' => $room]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reservation $reservation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservation $reservation)
    {
        //
    }

    public function getRoomDetailsId($id){
        $rooms = Room::with(['reservations' => function($query){
          return $query->whereNull('status')->orWhere('status', '=', 'arrived');
      }])
      ->find($id);


      foreach($rooms->reservations as $key => $reservation){
          $dates[] = $this->getDatesFromRange($reservation->check_in, $reservation->check_out);
      }
      return response()->json($dates);
    }

    public function getDatesFromRange($start, $end, $format = 'Y-m-d') {
        $array = array();
        $interval = new DateInterval('P1D');

        $realEnd = new DateTime($end);
        $realEnd->add($interval);

        $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

        foreach($period as $date) {
            $array[] = $date->format($format);
        }

        return $array;
    }


    // Paypal start here

    public function paymentSuccess(Request $request){



        $provider = new ExpressCheckout;
        $token = $request->token;
        $PayerID = $request->PayerID;
        $invoiceId = $response['INVNUM'] ?? uniqid();

        $room = RoomDetails::with('rooms')->find(Session::get('room_detail_id'));
        $data = $this->cartData($invoiceId, 'sample', Session::get('total_cost'), 1);



        // Get Details
        $response = $provider->getExpressCheckoutDetails($token);
        // Do Payment


        $data = $this->cartData($invoiceId, Session::get('room_name'), Session::get('total_cost'), 1);
        $response = $provider->doExpressCheckoutPayment($data, $token, $PayerID);
        // 3% paypal Fee


        if($response['ACK'] == "Success"){
            // dd($response);
            $customer = Customer::firstOrCreate([
                'name' => Session::get('firstname') . " " . Session::get('lastname'),
                'email' => Session::get('email'),
                'nationality' => Session::get('nationality'),
                'contact_no' => Session::get('contact'),

            ]);



            $reservation = $customer->reservations()->create([
                'room_id' => Session::get('room_id'),
                'room_details_id' => Session::get('room_detail_id'),
                'check_in' => Session::get('checkIn'),
                'check_out' => Session::get('checkOut'),
                'request' => Session::get('request'),
                'payment' => Session::get('paymentMethod'),
                'amount' => Session::get('total_cost'),
                'package_id' =>Session::get('packageID')
            ]);;

            $reservation->paypal_payments()->create([
                'status' => $response['PAYMENTINFO_0_PAYMENTSTATUS'],
                'paypal_fee' => $response['PAYMENTINFO_0_FEEAMT'],
                'invoice_id' => $invoiceId,
                'transaction_id' => $response['PAYMENTINFO_0_TRANSACTIONID'],
                'token' => $response['TOKEN'],
                'amount' => $response['PAYMENTINFO_0_AMT'],
                'type' => $response['PAYMENTINFO_0_PAYMENTTYPE']
            ]);


            $name = Session::get('firstname') . " " . Session::get('lastname');
            $number = Session::get('contact');
            $beautymail = app()->make(\Muratbsts\MailTemplate\MailTemplate::class);
            $beautymail->send('emails.welcome',
            [
                'name' =>$name,
                'number'=>$number,

            ],

             function($message)
            {
                $message
                    ->from('havenwellresort@gmail.com')
                    ->to(Session::get('email'))
                    ->subject('Welcome!');
            });


            Session::forget(['firstname','lastname','email','contact','room_detail_id','checkIn','checkOut', 'request', 'room_name','total_cost','paymentMethod']);

            return redirect('/')->with('message', 'Success');
        }


    }




    protected function cartData($invoiceId, $name, $price, $qty){

        $data['items'] = [

            [
                'name' => $name,
                'price' => $price,
                'qty' => $qty
            ]
        ];

        $data['invoice_id'] = $invoiceId;
        $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
        $data['return_url'] = url('/payment/success');
        $data['cancel_url'] = url('/');

        $total = 0;
        foreach($data['items'] as $item) {
            $total += $item['price']*$item['qty'];
        }

        $data['total'] = $total;
        return $data;

    }

    public function sessionDestroy(){
        return Session::all();

    }




    public function notify(Request $request)
    {
        if (!($this->provider instanceof ExpressCheckout)) {
            $this->provider = new ExpressCheckout();
        }
        $post = [
            'cmd' => '_notify-validate',
        ];
        $data = $request->all();
        foreach ($data as $key => $value) {
            $post[$key] = $value;
        }
        $response = (string) $this->provider->verifyIPN($post);
        $ipn = new IPNStatus();
        $ipn->payload = json_encode($post);
        $ipn->status = $response;
        $ipn->save();
        dd($response);
    }

    public function updateCheckinStatus($id,$check){

        $reservation =  Reservation::findOrFail($id);
        // return $reservation->room_details_id;

        $room =  RoomDetails::where('room_id',$reservation->room_id)->first();
        $room_book = Room::where('id',$reservation->room_id)->first();

        $total =  $room_book->total_booking + 1;
        date_default_timezone_set('Asia/Manila');
        $date = date('Y-m-d H:i:s');
        if($check == 'Check in'){

            $reservation->arrival = $date;
            $reservation->update();
        }else{
            $reservation->status = $check;
            $reservation->departure = $date;
            $reservation->update();

            $room_book->total_booking = $total;
            $room_book->update();
        }

        $reservation->status = $check;
        $reservation->update();

        return $reservation->status;
    }

    public function reserved()
    {
        return Reservation::where('status','Reserved')->get();
    }
    public function booked()
    {
        return Reservation::where('status','Done')->get();
    }
    public function searchforResearved(Request $request){

        return Reservation::with('customer','paypal_payments')
        ->where('status','Reserved')
        ->where('room_no', 'like', '%'. $request->value.'%')
        ->paginate(2);
    }
    public function detialsofResearved($val)
    {
        // return $val;
       return Reservation::with('customer','paypal_payments')->Where('id',$val)->first();
    }

    public function bookedRoom($roomID){
        $room =  Room::with('reserved')->Where('id',$roomID)->first();
        $dates = [];
        foreach($room->reserved as $key => $reserved){
            $dates[] = $this->getDatesFromRange($reserved->check_in, $reserved->check_out);
        }

        return response()->json(['dates' => $dates]);
    }


}
