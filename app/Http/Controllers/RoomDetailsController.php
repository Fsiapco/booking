<?php

namespace App\Http\Controllers;

use App\RoomDetails;
use Illuminate\Http\Request;
use App\Package;
use App\PackageDetail;
use App\Room;
class RoomDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RoomDetails  $roomDetails
     * @return \Illuminate\Http\Response
     */
    public function show(RoomDetails $roomDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RoomDetails  $roomDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(RoomDetails $roomDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RoomDetails  $roomDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RoomDetails $roomDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RoomDetails  $roomDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(RoomDetails $roomDetails)
    {
        //
    }

    public function packageDetails($id){
        return Package::with('package_details')->where('package_id',$id)->first();
    }

    public function roomDetails($id){
        return Room::with('room_details','room_amenities')->where('id',$id)->first();
    }

}
