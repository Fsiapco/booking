<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageDetail extends Model
{
    protected $guarded = [];

    public function item()
    {
        return $this->hasMany('App\item', 'product_id', 'id');
    }

    public function package(){
        return $this->belongsTo('App\Package', 'package_id', 'package_id');
    }


}
