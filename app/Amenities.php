<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Auth;
class Amenities extends Model
{



    protected $guarded = [];

    public function room_amenities_list(){
        return $this->hasMany('App\RoomAmenities', 'id', 'amenities_id');
    }

    public function branch(){
        return $this->belongsTo('App\Branch', 'branch_id', 'id');
    }

    public function getDescriptionForEvent(string $eventName): string
    {
      
        return $eventName;
    }
    public function getLogNameToUse():string 
    {
        $name = Auth::user()->name;
        return $name;
    }
}


