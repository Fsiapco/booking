<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaypalPayment extends Model
{
   

    protected $guarded = [];
    protected $table = 'paypal_payment';

    public function reservations(){
        return $this->belongsTo(Reservation::class, 'reservation_id', 'id');
    }

   
}
