<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Reservation;
class Room extends Model
{
   
    
    protected $guarded = [];


    public function room_details(){
        return $this->hasMany('App\RoomDetails');
    }

    public function room_amenities(){
        return $this->hasMany('App\RoomAmenities');
    }

    public function room_images(){
        return $this->hasMany('App\RoomImages');
    }
    public function reservations(){
        return $this->belongsTo(Room::class, 'room_details_id', 'id');
    }
    public function reserved(){
        return $this->hasMany(Reservation::class, 'room_id', 'id');
    }
    public function getDescriptionForEvent(string $eventName): string
    {
        return $eventName;
    }
    public function getLogNameToUse():string 
    {
        return "User" ;
    }
   
    public function branch(){
        return $this->belongsTo('App\Branch');
    }



}
