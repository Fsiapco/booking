<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomImages extends Model
{

    protected $guarded = [];

    public function rooms(){
        return $this->belongsTo('App\Room');
    }

}
