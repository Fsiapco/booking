<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Room;
use App\RoomDetails;
class Reservation extends Model
{

    protected $guarded = [];
    public function customer(){
        return $this->belongsTo('App\Customer');
    }

    public function rooms(){
        return $this->belongsTo(RoomDetails::class, 'room_details_id', 'id');
    }

    public function bookedRoom(){
        return $this->hasMany(room::class, 'id', 'room_id');
    }

    public function paypal_payments(){
        return $this->hasMany(PaypalPayment::class, 'reservation_id', 'id');
    }
}
