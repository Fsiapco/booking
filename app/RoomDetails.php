<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomDetails extends Model
{
    
    protected $guarded = [];
    
    public function rooms(){
        return $this->belongsTo('App\Room');
    }

    public function reservations(){
        return $this->hasMany('App\Reservation');
    }
}
