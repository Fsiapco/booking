<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Insert extends Model
{
    protected $guarded = [];
 
    protected $casts = [
        'options' => 'array'
    ];
}
