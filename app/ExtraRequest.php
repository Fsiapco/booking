<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtraRequest extends Model
{
    protected $guarded = [];
}