<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $guarded = [];

     public function package_details()
    {
        return $this->hasMany('App\PackageDetail', 'package_id', 'id');
    }

    public function package(){
        return $this->hasMany('App\Package', 'package_id', 'id');
    }
    public function branch(){
        return $this->belongsTo('App\Branch', 'branch_id', 'id');
    }

}
