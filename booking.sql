-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 03, 2020 at 07:17 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `booking`
--

-- --------------------------------------------------------

--
-- Table structure for table `amenities`
--

CREATE TABLE `amenities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `branch_id` int(250) NOT NULL,
  `amenities` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `amenities`
--

INSERT INTO `amenities` (`id`, `branch_id`, `amenities`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'internet', 'Activated', '2020-02-17 23:01:10', '2020-02-17 23:01:12'),
(2, 2, 'bed', 'Activated', '2020-02-17 23:01:39', '2020-02-17 23:01:41'),
(3, 1, 'internet', 'Activated', '2020-02-17 23:12:09', '2020-02-17 23:12:12'),
(4, 1, 'QWEQE', 'Activated', '2020-02-17 23:12:21', '2020-02-17 23:12:25');

-- --------------------------------------------------------

--
-- Table structure for table `attractions`
--

CREATE TABLE `attractions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attractions`
--

INSERT INTO `attractions` (`id`, `img`, `name`, `details`, `price`, `link`, `created_at`, `updated_at`) VALUES
(12, '1582163390.jpeg', 'teste', '232qwe', NULL, 'https://www.w3schools.com/tags/att_a_href.asp', '2020-02-19 17:40:48', '2020-02-19 17:50:04');

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `branch_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `branch_name`, `branch_address`, `created_at`, `updated_at`) VALUES
(1, 'Garden Resort', 'sample', '2020-01-26 16:00:00', '2020-01-26 16:00:00'),
(2, 'Beach Resort', 'sample', '2020-01-26 16:00:00', '2020-01-26 16:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `caterings`
--

CREATE TABLE `caterings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `caterings`
--

INSERT INTO `caterings` (`id`, `img`, `name`, `created_at`, `updated_at`) VALUES
(17, '1582166193.jpeg', 'test', '2020-02-19 18:36:33', '2020-02-19 18:36:33');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `contact_number`, `message`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Jeric', 'mangao_jeric@yahoo.com', '09502327839', 'qwertyqwerty', '1', '2020-01-22 00:40:37', '2020-01-22 00:40:53');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `nationality`, `contact_no`, `created_at`, `updated_at`) VALUES
(4, 'test test', 'fsiapco@gmail.com', 'Filipino', '09061237968', '2020-01-28 22:51:40', '2020-01-28 22:51:40'),
(5, 'test test', 'fsiapco@gmail.com', 'Filipino', '09061237968', '2020-01-28 22:53:21', '2020-01-28 22:53:21'),
(6, 'test test', 'fsiapco@gmail.com', 'Filipino', '09061237968', '2020-01-28 23:06:45', '2020-01-28 23:06:45'),
(7, 'test test', 'fsiapco@gmail.com', 'Filipino', '09061237968', '2020-01-28 23:28:51', '2020-01-28 23:28:51'),
(8, 'test test', 'fsiapco@gmail.com', 'Filipino', '09061237968', '2020-01-29 00:28:26', '2020-01-29 00:28:26'),
(9, 'test test', 'fsiapco@gmail.com', 'Filipino', '09061237968', '2020-01-29 18:53:10', '2020-01-29 18:53:10'),
(11, 'eqwqe qweqw', 'qeqwe@gmail.com', 'wrwwerwer', '34535535355', '2020-02-23 23:32:14', '2020-02-23 23:32:14'),
(12, 'weer werwer', 'were@gmail.com', 'rwrrwre', '31331231331', '2020-02-23 23:33:41', '2020-02-23 23:33:41'),
(13, 'enan siapco', 'fsiapco@gmail.com', 'test', '09061237968', '2020-02-26 01:04:49', '2020-02-26 01:04:49');

-- --------------------------------------------------------

--
-- Table structure for table `extra_requests`
--

CREATE TABLE `extra_requests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `extra_requests`
--

INSERT INTO `extra_requests` (`id`, `name`, `price`, `created_at`, `updated_at`) VALUES
(1, 'Extra Towel', 100, '2020-01-28 16:00:00', '2020-02-19 17:28:05'),
(2, 'Extra Bed', 200, '2020-01-28 16:00:00', '2020-01-21 16:00:00'),
(3, 'Van Transport', 300, '2020-01-28 16:00:00', '2020-01-21 16:00:00'),
(10, 'Extra kumot', 100, '2020-02-04 18:34:49', '2020-02-04 18:34:49');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `itemname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `itemname`, `price`, `type`, `created_at`, `updated_at`) VALUES
(46, 'Breakfast', '100', 'item', '2020-02-12 22:39:57', '2020-02-12 22:39:57'),
(47, 'Lunch', '200', 'item', '2020-02-12 22:40:11', '2020-02-12 22:40:11'),
(48, 'Dinner', '213', 'item', '2020-02-12 22:40:20', '2020-02-12 22:40:20'),
(53, 'Bag', '1231', 'item', '2020-02-12 23:12:00', '2020-02-12 23:12:00'),
(54, 'Phone', '1231', 'item', '2020-02-12 23:12:06', '2020-02-12 23:12:06'),
(55, 'Mug', '123', 'item', '2020-02-12 23:12:22', '2020-02-12 23:12:22'),
(59, 'Computer', '421', 'item', '2020-02-12 23:18:29', '2020-02-12 23:18:29'),
(60, 'CPU', '3123', 'item', '2020-02-12 23:18:38', '2020-02-12 23:18:38'),
(61, 'Keyboard', '2311', 'item', '2020-02-12 23:18:55', '2020-02-12 23:18:55'),
(69, 'test package', '1', 'package', '2020-02-19 17:52:36', '2020-02-19 17:52:36'),
(70, 'package 2', '1', 'package', '2020-03-01 19:51:48', '2020-03-01 19:51:48');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_05_09_070111_create_rooms_table', 1),
(9, '2019_05_09_070225_create_room_details_table', 1),
(10, '2019_05_09_072318_create_reservations_table', 1),
(11, '2019_05_09_072332_create_customers_table', 1),
(12, '2019_05_28_055031_create_paypal_payment_table', 1),
(13, '2019_05_31_040027_create_room_amenities_table', 1),
(14, '2019_06_13_060802_create_amenities_table', 1),
(15, '2019_07_04_051527_create_other_room_photos_table', 1),
(16, '2019_09_18_022740_add_room_details_id_to_reservations', 2),
(17, '2019_09_18_024442_add_room_no_to_reservations', 3),
(18, '2019_09_26_051828_create_contact_us_table', 4),
(19, '2019_10_01_050518_create_room_images_table', 5),
(20, '2019_10_10_005228_create_activity_log_table', 6),
(21, '2019_10_11_004133_add_login_fields_to_users_table', 7),
(22, '2020_01_27_060658_create_branches_table', 8),
(23, '2020_01_27_060956_create_branches_table', 9),
(24, '2020_01_29_030111_create_requests_table', 10),
(25, '2020_01_29_031349_create_extra_requests_table', 11),
(26, '2020_02_06_065641_create_packages_table', 12),
(27, '2020_02_06_070050_create_items_table', 12),
(28, '2020_02_07_030444_create_package_details_table', 13),
(29, '2020_02_11_055335_create_caterings_table', 14),
(30, '2020_02_12_032127_create_attractions_table', 15);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'JoUSgqqlpQKUqDw0wY6UGk9mRCm0jQX225H0NIJ1', 'home', 0, 0, 1, '2019-09-18 16:43:30', '2019-09-18 16:49:10');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `other_room_photos`
--

CREATE TABLE `other_room_photos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `room_id` int(11) NOT NULL,
  `room_photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `package_id` int(250) NOT NULL,
  `img` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `packagename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_price` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `package_id`, `img`, `packagename`, `details`, `total_price`, `created_at`, `updated_at`) VALUES
(22, 69, '1582163572.jpeg', 'test package', 'test', 5434.00, '2020-02-19 17:52:52', '2020-02-19 17:52:52'),
(23, 70, '1583121157.jpeg', 'package 2', 'test', 3667.00, '2020-03-01 19:52:37', '2020-03-01 19:52:37');

-- --------------------------------------------------------

--
-- Table structure for table `package_details`
--

CREATE TABLE `package_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `package_id` int(11) NOT NULL,
  `Itemname` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(250) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `package_details`
--

INSERT INTO `package_details` (`id`, `package_id`, `Itemname`, `price`, `created_at`, `updated_at`) VALUES
(133, 69, 'Keyboard', 2311, '2020-02-19 17:52:41', '2020-02-19 17:52:41'),
(134, 69, 'CPU', 3123, '2020-02-19 17:52:41', '2020-02-19 17:52:41'),
(135, 70, 'CPU', 3123, '2020-03-01 19:51:56', '2020-03-01 19:51:56'),
(136, 70, 'Computer', 421, '2020-03-01 19:51:57', '2020-03-01 19:51:57'),
(137, 70, 'Mug', 123, '2020-03-01 19:51:57', '2020-03-01 19:51:57');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `paypal_payment`
--

CREATE TABLE `paypal_payment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reservation_id` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'express',
  `amount` double NOT NULL,
  `paypal_fee` double NOT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `paypal_payment`
--

INSERT INTO `paypal_payment` (`id`, `reservation_id`, `status`, `invoice_id`, `type`, `amount`, `paypal_fee`, `transaction_id`, `token`, `created_at`, `updated_at`) VALUES
(18, 18, 'Completed', '5e324996eabdd', 'instant', 922.5, 22.44, '0RV55644GA263860G', 'EC-6DV38844H1735013G', '2020-01-29 19:12:28', '2020-01-29 19:12:28'),
(19, 19, 'Completed', '5e324a1963810', 'instant', 1845, 44.58, '8HY965817W6708727', 'EC-8JN96435FY7108354', '2020-01-29 19:14:40', '2020-01-29 19:14:40'),
(20, 20, 'Completed', '5e3266336143a', 'instant', 324.72, 8.09, '07S420777K019004C', 'EC-7F681389W7344425P', '2020-01-29 21:14:33', '2020-01-29 21:14:33'),
(21, 21, 'Completed', '5e326781dedce', 'instant', 922.5, 22.44, '4YT64098JJ5677506', 'EC-1M536537K1000260A', '2020-01-29 21:20:08', '2020-01-29 21:20:08'),
(22, 22, 'Completed', '5e326e7500a7b', 'instant', 56.18, 1.65, '6JA06752C93209245', 'EC-4KJ03786FG0029909', '2020-01-29 21:49:46', '2020-01-29 21:49:46'),
(23, 24, 'Completed', '5e560f56cf984', 'instant', 1269.99, 30.78, '0MB78516EB526683Y', 'EC-1F232584KV991991L', '2020-02-25 22:25:32', '2020-02-25 22:25:32'),
(24, 25, 'Completed', '5e5617ec74c2d', 'instant', 2392.69, 57.72, '2VJ95334JE145300R', 'EC-7TA21344F6452043L', '2020-02-25 23:02:09', '2020-02-25 23:02:09'),
(25, 26, 'Completed', '5e56337eefa5b', 'instant', 1269.99, 30.78, '4GL770076G803414K', 'EC-1C081395MV293411J', '2020-02-26 00:59:48', '2020-02-26 00:59:48'),
(26, 27, 'Completed', '5e563a589d310', 'instant', 43472, 1043.63, '7KJ85106T5055131L', 'EC-77X41972EX590412G', '2020-02-26 01:29:04', '2020-02-26 01:29:04'),
(27, 28, 'Completed', '5e563e6d5ca46', 'instant', 10868, 261.13, '1P443289JM855942W', 'EC-7PK65997Y5852401E', '2020-02-26 01:46:27', '2020-02-26 01:46:27'),
(28, 29, 'Completed', '5e56411289b25', 'instant', 54340, 1304.46, '6H0472830K215213W', 'EC-77B54318EJ602245K', '2020-02-26 01:57:43', '2020-02-26 01:57:43'),
(29, 30, 'Completed', '5e5c5cc815247', 'instant', 8151, 195.92, '479232053S921670C', 'EC-17S6770946832704D', '2020-03-01 17:09:33', '2020-03-01 17:09:33'),
(30, 31, 'Completed', '5e5cc4d2b521c', 'instant', 1233, 29.89, '45U32943AV047942W', 'EC-3YN134722G583563K', '2020-03-02 00:33:28', '2020-03-02 00:33:28'),
(31, 32, 'Completed', '5e5de6ec538e3', 'instant', 3068, 73.93, '7J161060TT307640T', 'EC-5SB431363M1840049', '2020-03-02 21:11:14', '2020-03-02 21:11:14');

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `room_details_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `package_id` int(250) DEFAULT NULL,
  `check_in` date NOT NULL,
  `check_out` date NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Reserved',
  `arrival` timestamp NULL DEFAULT NULL,
  `departure` timestamp NULL DEFAULT NULL,
  `request` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` float NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`id`, `room_id`, `room_details_id`, `customer_id`, `package_id`, `check_in`, `check_out`, `status`, `arrival`, `departure`, `request`, `payment`, `amount`, `created_at`, `updated_at`) VALUES
(26, 43, 38, 4, NULL, '2020-02-26', '2020-02-27', 'Reserved', NULL, NULL, NULL, 'Full Payment', 1269.99, '2020-02-26 00:59:48', '2020-02-26 00:59:48'),
(27, 43, 38, 13, NULL, '2020-03-19', '2020-03-27', 'Reserved', NULL, NULL, NULL, 'Full Payment', 43472, '2020-02-26 01:29:04', '2020-02-26 01:29:04'),
(28, 43, 38, 13, NULL, '2020-02-26', '2020-02-28', 'Reserved', NULL, NULL, NULL, 'Full Payment', 10868, '2020-02-26 01:46:27', '2020-02-26 01:46:27'),
(29, 43, 39, 13, 69, '2020-03-08', '2020-03-18', 'Reserved', NULL, NULL, 'None', 'Full Payment', 54340, '2020-02-26 01:57:43', '2020-02-26 01:57:43'),
(30, 43, 38, 13, 69, '2020-03-28', '2020-03-31', 'Reserved', NULL, NULL, 'None', 'Partial Payment', 8151, '2020-03-01 17:09:33', '2020-03-01 17:09:33'),
(31, 43, 38, 13, NULL, '2020-03-02', '2020-03-04', 'Reserved', NULL, NULL, 'None', 'Full Payment', 1233, '2020-03-02 00:33:28', '2020-03-02 00:33:28'),
(32, 45, 40, 13, NULL, '2020-03-03', '2020-03-05', 'Reserved', NULL, NULL, 'Extra Towel,Extra Bed,Van Transport', 'Full Payment', 3068, '2020-03-02 21:11:14', '2020-03-02 21:11:14');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `branch_id` int(11) NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `guests` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_booking` int(11) DEFAULT 0,
  `queenbed` int(250) DEFAULT NULL,
  `singlebed` int(250) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `branch_id`, `photo`, `type`, `price`, `guests`, `description`, `total_booking`, `queenbed`, `singlebed`, `created_at`, `updated_at`) VALUES
(43, 1, '1582161995.jpeg', '1233', 1233, '3', '105', 0, 1, 0, '2020-02-19 17:26:35', '2020-03-02 01:23:19'),
(44, 1, '1582178797.png', '123', 2323, '2', '101', 0, 0, 1, '2020-02-19 22:06:37', '2020-03-02 01:23:08'),
(45, 2, '1583140311.jpeg', 'room3', 1234, '2', 'qeqweqewqew', 0, 1, 1, '2020-03-02 01:11:51', '2020-03-02 21:25:23');

-- --------------------------------------------------------

--
-- Table structure for table `room_amenities`
--

CREATE TABLE `room_amenities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `amenities` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amenities_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `room_amenities`
--

INSERT INTO `room_amenities` (`id`, `amenities`, `amenities_id`, `room_id`, `created_at`, `updated_at`) VALUES
(134, 'internet', '3', 44, '2020-03-02 01:23:08', '2020-03-02 01:23:08'),
(135, 'internet', '1', 43, '2020-03-02 01:23:19', '2020-03-02 01:23:19'),
(138, 'internet', '3', 45, '2020-03-02 21:25:23', '2020-03-02 21:25:23');

-- --------------------------------------------------------

--
-- Table structure for table `room_details`
--

CREATE TABLE `room_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `room_id` int(11) NOT NULL,
  `room_no` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `room_details`
--

INSERT INTO `room_details` (`id`, `room_id`, `room_no`, `created_at`, `updated_at`) VALUES
(38, 43, 105, '2020-02-19 17:26:35', '2020-03-02 01:23:19'),
(39, 44, 101, '2020-02-19 22:06:37', '2020-03-02 01:23:08'),
(40, 45, 123, '2020-03-02 01:11:51', '2020-03-02 21:25:23');

-- --------------------------------------------------------

--
-- Table structure for table `room_images`
--

CREATE TABLE `room_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `room_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `branch_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_login_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `branch_id`, `name`, `firstname`, `lastname`, `email`, `photo`, `email_verified_at`, `password`, `status`, `remember_token`, `created_at`, `updated_at`, `last_login_at`) VALUES
(2, 3, 'Enan', 'enan', 'siapco', 'fsiapco@gmail.com', '1580955746.jpeg', NULL, '$2y$10$p8l2iFDZPdq7k2iEwn1EW.yAMbj2VR487Tg.z2n2SLS8RfV9w9Jpu', 'Activated', NULL, '2019-09-19 00:06:06', '2020-03-02 21:56:54', '2020-03-03 13:56:54'),
(3, 2, 'irish', 'irish', 'Gorospes', 'irish.gorospe@gmail.com', '1582097353.jpeg', NULL, '$2y$10$tRZCD/UhARtSV97uKopgYeWEO39hXhOzdKJECZ3ULOqyLgLw1Mpru', 'Deactivated', NULL, '2019-09-22 16:34:12', '2020-02-18 23:29:14', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `amenities`
--
ALTER TABLE `amenities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attractions`
--
ALTER TABLE `attractions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `caterings`
--
ALTER TABLE `caterings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `extra_requests`
--
ALTER TABLE `extra_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `other_room_photos`
--
ALTER TABLE `other_room_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_details`
--
ALTER TABLE `package_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `paypal_payment`
--
ALTER TABLE `paypal_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_amenities`
--
ALTER TABLE `room_amenities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_details`
--
ALTER TABLE `room_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_images`
--
ALTER TABLE `room_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `amenities`
--
ALTER TABLE `amenities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `attractions`
--
ALTER TABLE `attractions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `caterings`
--
ALTER TABLE `caterings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `extra_requests`
--
ALTER TABLE `extra_requests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `other_room_photos`
--
ALTER TABLE `other_room_photos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `package_details`
--
ALTER TABLE `package_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT for table `paypal_payment`
--
ALTER TABLE `paypal_payment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `room_amenities`
--
ALTER TABLE `room_amenities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `room_details`
--
ALTER TABLE `room_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `room_images`
--
ALTER TABLE `room_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
