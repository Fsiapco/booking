$(document).ready(function(){

    $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z]+$/i.test(value);
      }, "Letters only please"); 

    $('#gardenRoom').validate({
        rules:{
            dates:{
                required:true,   
            },
            firstname:{
                required:true,
                lettersonly:true
            },
            lastname:{
                required:true,
                lettersonly:true
            },
            contact_number:{
                required:true,
                digits:true,
                minlength:11,
                maxlength:11
            },
            nationality:{
                required:true,
                lettersonly:true
            },
            email:{
                required:true,
                email:true
            },
            SelectedRoom:{
                required:true,
                
            },
        }
    });
    
});