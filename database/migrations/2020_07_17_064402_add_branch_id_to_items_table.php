<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBranchIdToItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
             // 1. Create new column
            // You probably want to make the new column nullable
            $table->integer('branch_id')->unsigned()->nullable()->after('type');

            // 2. Create foreign key constraints
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            // 1. Drop foreign key constraints
            $table->dropForeign(['branch_id']);

            // 2. Drop the column
            $table->dropColumn('branch_id');
        });
    }
}
